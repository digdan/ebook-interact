
			//Here is the initializer script for the full width image rotator
			jQuery(function($){
				$.supersized({
					slides  :  	[
					{image : 'images/image1.jpg'},
                    {image : 'images/image2.jpg'},
                    {image : 'images/image3.jpg'},
                    {image : 'images/image4.jpg'}
					]
				});
		    });

            //The script of the Jquery email validation
		    $(document).ready(function(e) {
                $('input.submit').click(function() {
                    var sEmail = $('input.email').val();
                    if ($.trim(sEmail).length == 0) {
                    	$('p.good,p.bad').css({'visibility':'hidden','position':'absolute'});
                        $('form.subscribe').append('<p class="error one">Please enter a valid email address</p>');
                        event.preventDefault();//stops script execution
                        return false;
                    }
                    if (validateEmail(sEmail)) {
                    	$('p.error,p.bad').css({'visibility':'hidden','position':'absolute'});
                        $('form.subscribe').append('<p class="good">Email is valid</p>');
                    } else {
                    	$('p.error,p.good').css({'visibility':'hidden','position':'absolute'});
                        $('form.subscribe').append('<p class="bad">Invalid Email Address.Please try again</p>');
                        event.preventDefault();
                        return false;
                    }
                });
            });

            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {return true;}
            else {return false;}
            }


		// COUNTDOWN OPTIONS //
        $(function () {
 	            $('#countdown').countdown({until:$.countdown.UTCDate

 	            // Set the date to countdown to
                //Don't be scared by -0, -1, just edit the rest
 	            (-0, 2015,  6 , 1),
 	            // Set the URL to load when the countdown reaches 0 0 0 0
 	            // expiryUrl:'http://yournewsite.com',

 	            //END OPTIONS
 	            format: 'dHMS', layout:
                '<span class="numbers">' +
	                '<span id="timer_days" class="timer_numbers days">{dnn}</span>'+
	                '<span id="timer_hours" class="timer_numbers">{hnn}</span>'+
	                '<span id="timer_mins" class="timer_numbers">{mnn}</span>'+
	                '<span id="timer_seconds" class="timer_numbers">{snn}</span>'+
                    '<span class="labels">'+
	                    '<span id="timer_days_label" class="timer_labels">days</span>'+
	                    '<span id="timer_hours_label" class="timer_labels">hours</span>'+
	                    '<span id="timer_mins_label" class="timer_labels">minutes</span>'+
	                    '<span id="timer_seconds_label" class="timer_labels">seconds</span>'+
                    '</span>'+
                '</span>'
                });
            });

        //This is for the twitter part
        $(document).ready(function() {
            $('#tweets').jtwt({
                image_size : 24,
                count : 2, //Set the count to 1 or 3 depending on the size of your tweets
                username: 'ebookinteract', //Change the username
                convert_links : 1,
                loader_text : ''
            });
        });

        //For the Less more button
        $(document).ready(function() {
            $("a.more").click(function() {
                $("div.footer").css({'bottom':'0'}).show(1000);
                $('a.more').fadeTo('slow', 0);
                $('.main-wrapper').fadeTo('slow', .2);
            });
            $("a.less").click(function() {
                $("div.footer").hide("slow").css({'bottom':'-600px'});
                $('a.more').fadeTo('slow', 1);
                $('.main-wrapper').fadeTo('slow', 1);
            });
        });


        //For the Contact form
         $(document).ready(function(e) {
                $('input.cf').click(function() {
                    var sCE = $('input.contactemail').val();
                    if ($.trim(sCE).length == 0) {
                        $('p.good,p.bad').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="error one">Please enter a valid email address</p>');
                        event.preventDefault();//stops script execution
                        return false;
                    }
                    if (validateEmail(sCE)) {
                        $('p.error,p.bad').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="good">Email is valid</p>');
                    } else {
                        $('p.error,p.good').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="bad">Invalid Email Address.Please try again</p>');
                        event.preventDefault();
                        return false;
                    }
                });
            });

            $(document).ready(function(e) {
                $('input.cf').click(function() {
                    var sCN = $('input.contactname').val().replace(/ /g,'');
                    if ($.trim(sCN).length == 0) {
                        $('p.ngood,p.nbad').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="nerror one">Please enter a valid name</p>');
                        event.preventDefault();//stops script execution
                        return false;
                    }
                    if (validateName(sCN)) {
                        $('p.nerror,p.nbad').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="ngood">Name is valid</p>');
                    } else {
                        $('p.nerror,p.ngood').css({'visibility':'hidden','position':'absolute'});
                        $('form.contact').append('<p class="nbad">Invalid name</p>');
                        event.preventDefault();
                        return false;
                    }
                });
            });


            function validateName(sCN) {
                var filter = /^([a-zA-Z]+)$/;
                if (filter.test(sCN)) {return true;}
            else {return false;}
            }

            function validateEmail(sCE) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sCE)) {return true;}
            else {return false;}
            }
