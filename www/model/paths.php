<?php
	class model_paths extends ORM {
		var $id;
		var $text;
		var $destination_node;
		var $destination_node_id;
		var $destination_chapter_id;
		var $destination_text;
		var $source_node_id;
		var $class; //CSS Class attached to path
		var $trigger_node_id;
		var $trigger_condition=true;
		var $position=1;

		function __construct($path_id=NULL) {
			$this->created = time();
			if (!is_null($path_id)) $this->loadFromID($path_id);
		}

		static function renderPaths(model_nodes $node) {
			$book = new model_books($node->book_id);
			$node_map = $book->componentMap();
			$content ="<ul class='options'>";
			$paths = $node->paths();
			foreach($paths as $path) {
				if ($path->destination_chapter_id > 0) {
					$dest_node_id = $node_map[ 'c' . $path->destination_chapter_id ]; //Chapter referances node
				} else {
					$dest_node = $node_map[ $path->destination_node_id ];
					$dest_node_id = $dest_node->id;
				}
				$dest = "node_".$dest_node_id.".xhtml";
				$class='';
				if (isset($path->class)) $class = $path->class;
				$content .= "<li class='option {$class}'><A HREF=\"{$dest}\">{$path->text}</A></li>";
			}
			$content .= "</ul>";
			//$content .= print_r($node_map,true);
			$content .= "<mbp:pagebreak/>";

			return $content;
		}

		static function fromNode($node_id=NULL) {
			$source = new model_nodes($node_id);
			$path_table = ORM::gettable('model_paths');
			$handle = DB::instance()->prepare("SELECT * FROM {$path_table} WHERE source_node_id = :node_id");
			$handle->execute(array(
				':node_id'=>$node_id
			));
			$out = array();
			while($res = $handle->fetch()) {
				if ($res["destination_node_id"]) {
					$nn = new model_nodes($res["destination_node_id"]);
					$nnc = new model_chapters($nn->chapter_id);
					if ($source->chapter_id != $nn->chapter_id) { //Different Chapter
						$res["destination_text"] = "Chapter {$nnc->order} : {$nnc->name}";
					} else { //Same Chapter
						$res["destination_text"] = "{$nn->name}";
					}
					$res["edit_url"] = Router::instance()->generate("books/node",array("id"=>$nnc->book_id,"node_id"=>$nn->id));
				} else {
					$nc = new model_chapters($res['destination_chapter_id']);
					$res["edit_url"] = Router::instance()->generate("books/node",array("id"=>$nc->book_id,"node_id"=>$nc->start_node_id()));
					$res["destination_text"] = "Chapter ".$nc->order." : ".$nc->name;
				}
				$out [] = $res;
			}
			return $out;
		}
	}
?>