<?php
class model_ebooks extends ORM {
	var $book_id;
	var $render_date;
	var $hash;
	var $build=1;
	var $filename;

	function save($allowedFields=NULL) {
		$this->build = self::maxBuild($this->book_id) + 1;
		$this->render_date = time();
		parent::save($allowedFields);
	}

	function hash($hash) {
		$this->hash = $hash;
	}

	function filename($filename) {
		$this->filename = $filename;
	}

	function setBook(model_books $in_book) {
		$this->book_id = $in_book->id;
	}

	static function loadLatest($book_id) {
		$build = self::maxBuild($book_id);
		return self::loadBuild($book_id,$build);
	}

	static function loadBuild($book_id,$build) {
		$table = self::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE book_id = :book_id and build = :build");
		$handle->execute(array(
			':book_id'=>$book_id,
			':build'=>$build
		));
		$res = $handle->fetch();
		return new self($res['id']);
	}

	static function maxBuild($book_id) {
		$table = self::getTable();
		$handle = DB::instance()->prepare("SELECT max(build) as max_id FROM {$table} WHERE book_id = :book_id");

		$handle->execute(array(
			':book_id'=>$book_id
		));

		$res = $handle->fetch();
		return $res['max_id'];
	}

	static function download($book_id,$build=NULL) {
		$book = new model_books($book_id);
		$ebook = (is_null($build) ? model_ebooks::loadLatest($book_id) : model_ebooks::loadBuild($book_id,$build));
		//Todo Statistics, Security, Payment Transactions, Nounces
		header("Content-type: application/epub+zip");
		header('Content-Disposition: attachment; filename="'.$ebook->filename.'"');
		$file = FKeeper::instance()->hashPath($ebook->hash);
		$fp = fopen($file,"rb");
		fpassthru($fp);
		fclose($fp);
		die();
	}
}
?>