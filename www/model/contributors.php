<?php
	class model_contributors {
		var $user_id;
		var $book_id;
		var $role;

		static function canContribute($book_id,$user_id=NULL) {
			if (is_null($user_id)) {
				$user_id = $_SESSION["user_id"];
			}

			$table = static::getTable();
			$handle = DB::instance()->prepare("SELECT * FROM {$table} WHERE book_id = :book_id AND user_id = :user_id");
			$handle->execute(array(
				':book_id'=>$book_id,
				':user_id'=>$user_id
			));
			return ($handle->numRows() > 0);
		}

		static function getRole($book_id,$user_id=NULL) {
			if (is_null($user_id)) {
				$user_id = $_SESSION["user_id"];
			}

			$table = static::getTable();
			$handle = DB::instance()->prepare("SELECT * FROM {$table} WHERE book_id = :book_id AND user_id = :user_id");
			$handle->execute(array(
				':book_id'=>$book_id,
				':user_id'=>$user_id
			));
			if ($handle->numRows() > 0) {
				$row = $handle->fetch();
				return $row["role"];
			} else {
				return false;
			}
		}

	}
?>