<?php
	class model_images extends ORM {
		var $data;
		var $sizes;
		var $user_id;
		var $book_id;
		var $mime;
		var $created;

		function __construct($id=NULL) {
			$this->created = time();
			return parent::__construct($id);
		}

		static function loadByBookCover($id) {
			$table = static::getTable();
			$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE book_id = ?");
			$handle->execute([$id]);
			if ($handle->rowCount() > 0) {
				$row = $handle->fetch();
				return static::instance($row['id']);
			}
			return false;
		}

	}
?>