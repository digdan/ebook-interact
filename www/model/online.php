<?php

/**
 * Class model_online
 *
 * Sessions
 *
 */
class model_online extends ORM {
	var $user_id;
	var $session_id;
	var $token;
	var $when;

	static function checkSession() {
		if (isset($_COOKIE["remember"]) and ($_COOKIE["remember"] != "")) { //Remember Me
			if ($session = static::instanceByToken($_COOKIE["remember"])) {
				$_SESSION["user_id"] = $session->user_id;
				$_SESSION["token"] = $_COOKIE["remember"];
			} else { //Session not found
				setcookie("remember",""); //Forget me
				return false;
			}
		} elseif ($user_id = @$_SESSION['user_id']) { //Session only
			$session = static::instanceByUserID($user_id);
			if ((session_id() == $session->session_id) && ($session->token == @$_SESSION['token'])) {
				$session->refreshSession();
			} else {
				session_destroy(); //Unable to carry session, Force logout
				$session->delete();
			}
		}
		return false;
	}

	static function instanceByUserID($user_id) {
		$table = static::getTable();
		$handle = DB::instance()->query("SELECT id FROM {$table} WHERE user_id = {$user_id}");
		if ($row = $handle->fetch()) {
			return static::instance($row['id']);
		} else {
			return false;
		}
	}

	static function instanceByToken($token) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE token = ?");
		$handle->execute(array($token));
		if ($row = $handle->fetch()) {
			return static::instance($row['id']);
		} else {
			return false;
		}
	}

	public function build(model_users $user=NULL,$remember=FALSE) {
		//Build the token
		$config = Config::instance();
		$random = $this->genRandomString();
		$token = $_SERVER['REMOTE_ADDR'] . $random;
		$token = $this->hashData($token);

		$this->user_id = $user->id;
		$this->setToken($token);
		$this->when = time();
		$this->session_id = session_id();

		//Setup sessions vars
		$_SESSION['token'] = $token;
		$_SESSION['user_id'] = $user->id;


		if ($remember === TRUE) { // Remember Me
			setcookie('remember',$token, time() + $config["session"]["remember_time"] );
			setcookie("remember",$token,0,'/',$config['cookie_domain']);
		} else {
			setcookie('remember','');
		}
	}

	public function logout() {
		$table = static::getTable();
		if ($user_id = static::user_id()) {
			DB::instance()->exec("DELETE FROM {$table} WHERE user_id = {$user_id}");
			session_destroy();
		}
		return true;
	}

	protected function refreshSession() {
		$config = Config::instance();
		$random = $this->genRandomString();
		$token = $_SERVER['REMOTE_ADDR'] . $random;
		$token = $this->hashData($token);
		if (isset($_COOKIE["remember"]) and ($_COOKIE["remember"] != "")) setcookie("remember",$token,0,'/',$config['cookie_domain']);
		$this->session_id = session_id();
		$this->setToken($token);
		$_SESSION['token'] = $token;
		$this->when = time();
		$this->save();
	}

	public function setToken($token) {
		$this->token = $token;
	}

	protected function genRandomString($length = 50) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = '';
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters) -1)];
		}

		return $string;
	}

	protected function hashData($data) {
		return hash_hmac('sha512',$data, \SITEKEY );
	}

}
?>