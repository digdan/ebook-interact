<?php
class model_nodes extends ORM {

	var $name;
	var $book_id;
	var $chapter_id;
	var $content;
	var $position=1;

	function __construct($id=NULL) {
		//Preloaded values
		$this->content = '';
		return parent::__construct($id);
	}

	function isStart() { //Is this a start node?
		$chapter = new model_chapters($this->chapter_id);
		if ($chapter->order == 1) return ($chapter->node_start_id == $this->id);
		return false;
	}

	function paths() {
		$path_table = ORM::gettable('model_paths');
		$handle = DB::instance()->prepare("SELECT id FROM {$path_table} WHERE source_node_id = :node_id");
		$handle->execute(array(
			':node_id'=>$this->id
		));
		$out = array();
		while($res = $handle->fetch()) {
			$out[] = new model_paths($res['id']);
		}
		return $out;
	}

	function content($paths) {
		$choice = model_paths::renderPaths($this);
		//TODO Book Templating
		return $this->content.$choice;
	}

	static function create( $book_id, $chapter_id, $name=NULL) {
		$book = model_books::instance($book_id);
		$chapter = new model_chapters($chapter_id);
		$new = new self();
		$new->book_id = $book->id;
		$new->chapter_id = $chapter->id;
		if (is_null($name)) {
			$new->name = 'New Node ';
		} else {
			$new->name = $name;
		}
		$new->save();
		$new->name = 'New Node #'.$new->id;
		$new->save();
		return $new;
	}

}
?>