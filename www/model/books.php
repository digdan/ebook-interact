<?php

class model_books extends ORM {
	var $user_id;
	var $title='';
	var $last_build=0;
	var $build_count=0;
	var $publish='closed';
	var $language='English';
	var $license='public';
	var $description;
	var $cover_image_id;
	var $node_start_id;
	var $comment_count=0;
	var $rating=0;
	var $rating_count=0;
	var $when=0;

	static function allBooksByUser($user_id) {
		$table = static::getTable();
		$books = array();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE user_id = ?");
		$handle->execute(
			array($user_id)
		);
		while($result = $handle->fetch()) {
			$books[$result["id"]] = new self($result["id"]);
		}
		return $books;
	}

	function __construct($id=NULL) {
		$this->when = time();
		return parent::__construct($id);
	}


	function isOwner($user_id=NULL) {
		if (is_null($user_id)) {
			$user_id = @$_SESSION["user_id"];
		}
		if (!$user_id) return false;
		return ($this->user_id == $user_id);
	}

	function allNodes($chapter_id=NULL) {
		$nodes = array();
		$table = static::getTable('model_nodes');
		$chapter_table = static::getTable('model_chapters');
		if (is_null($chapter_id)) {
			$handle = DB::instance()->prepare("SELECT {$table}.*,{$chapter_table}.node_start_id as start_node, {$chapter_table}.name as chapter_name FROM {$table},{$chapter_table} WHERE {$table}.book_id = ? and {$table}.chapter_id = {$chapter_table}.id");
		} else {
			$handle = DB::instance()->prepare("SELECT {$table}.*,{$chapter_table}.node_start_id as start_node, {$chapter_table}.name as chapter_name FROM {$table},{$chapter_table} WHERE {$table}.book_id = ? and {$table}.chapter_id = {$chapter_table}.id and {$table}.chapter_id = {$chapter_id}");
		}

		$handle->execute(array( $this->id ));
		while($res = $handle->fetch()) {
			if ($res["start_node"] == $res["id"]) {
				$res["start_node"] = true;
			} else {
				$res["start_node"] = false;
			}
			$nodes[] = $res;
		}
		return $nodes;

	}

	function canContribute($user_id=NULL,$role=NULL) {
		$table = static::getTable();
		if (is_null($user_id)) {
			$user_id = @$_SESSION["user_id"];
		}
		if (!$user_id) return false;

		if ($this->user_id == $user_id) return true;
		//check contribution line items
		$handle = DB::instance()->prepare("SELECT role FROM {$table} WHERE user_id = :user_id AND book_id = :book_id");
		$handle->execute(array(":user_id"=>$user_id,":book_id"=>$this->id));
		if ($handle->rowCount() > 0) {
			if (is_null($role)) return true;
			$row = $handle->fetch();
			return ($row['row'] == $role);
		}
	}

	function save($allowedFields = NULL) {
		if ($this->id == 0) { //New Book?
			parent::save($allowedFields);
			model_chapters::chapterAdd($this->id,'Starting Chapter');
		} else {
			// TODO Check if conditions are right for a rebuild
			$this->last_build = time();
			parent::save();
		}
	}

	function chapter_count() {
		$table = static::getTable('model_chapters');
		$res = DB::instance()->prepare("SELECT count(id) FROM {$table} WHERE book_id = ?")->execute(array( $this->id ))->fetch();
		return $res[0];
	}

	function node_count() {
		$table = static::getTable('model_nodes');
		$res = DB::instance()->prepare("SELECT count(id) FROM {$table} WHERE book_id = ?")->execute(array( $this->id ))->fetch();
		return $res[0];
	}

	function delete() {
		$table = static::getTable('model_chapters');
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE book_id = ?");
		$handle->execute(array( $this->id ));
		while ($res =  $handle->fetch()) {
			$delChapter = new model_chapters($res['id']);
			$delChapter->delete();
		}
		if ($this->cover_image_id > 0) {
			$image = new model_images($this->cover_image_id);
			$image->delete();
		}
		parent::delete();
	}

	function build() { //Build Book
		Session::mustAuth();

		$render = new model_ebooks();
		$render->setBook( $this );
		$builder = BookBuilder::generateBook( $this->id );
		$render->hash( $builder->render() );
		$render->filename( Utils::slugify( $this->title ).".epub");
		$render->save();

		$this->build_count = $render->build;
		$this->save();
	}

	function getAuthor() {
		return new model_users( $this->user_id );
	}

	function componentMap() { //Used to translate nodes/chapters into node files
		$map = array();
		$firstNode = array();
		foreach( model_chapters::chaptersFromBook($this->id,TRUE) as $chapter_id) {
			$chapter = new model_chapters($chapter_id);
			foreach( $chapter->nodesFromChapter() as $node) {
				if (!isset($map['c'.$chapter_id])) $map['c'.$chapter_id] = $node->id;
				$map[$node->id] = $node;
			}
		}
		return $map;
	}
}
?>
