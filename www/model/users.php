<?php

/**
 * Class model_users
 *
 *
 *
 */
class model_users extends ORM {
	var $email;
	var $password;
	var $user_salt;
	var $is_verified=0;
	var $is_active=1;
	var $is_admin=0;
	var $verification_code;
	var $display_name;

	var $avatar_url;
	var $bio;

	const LOGGED_IN = 1;
	const PASSWORD_WRONG = 2;
	const NOT_VERIFIED = 3;
	const NOT_ACTIVE = 4;
	const LOGIN_ERROR = 5;
	const NOT_FOUND = 6;

	static function instanceByEmail($email) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE email=?");
		$handle->execute([ $email ]);
		if ($handle->rowCount() == 0) return false;
		$row = $handle->fetch();
		return static::instance($row['id']);
	}

	static function instanceFromSession() {
		if (isset($_SESSION['user_id'])) {
			return static::instance($_SESSION['user_id']);
		} else {
			return false;
		}
	}

	static function login($email,$password,$remember=false) {
		if ($user = self::instanceByEmail($email)) {
			$match = $user->matchPassword($password);
			if ($match === TRUE) {
				if ($user->is_verified) {
					if ($user->is_active) {
						return static::LOGGED_IN;
						//return static::LOGIN_ERROR;
					} else {
						return static::NOT_ACTIVE;
					}
				} else {
					return static::NOT_VERIFIED;
				}
			} else {
				return static::PASSWORD_WRONG;
			}
		} else {
			return static::NOT_FOUND;
		}
	}

	function __construct($id=NULL) {
		$this->verification_code = $this->genRandomString();
		return parent::__construct($id);
	}

	protected function build() {
		$this->resetVerification();
	}
	
	public function resetVerification() {
		$this->verification_code = $this->genRandomString();
		$this->save();
		return $this->verfication_code;
	}

	public function setPassword($password) {
		$this->user_salt = $this->genRandomString();
		$password = $this->user_salt . $password;
		$this->password = $this->hashData($password);
		return true;
	}

	public function setAvatar($avatar_url) {
		$this->avatar_url = $avatar_url;
		return true;
	}

	public function setBio($bio) {
		$this->bio = $bio;
		return true;
	}

	public function setDisplayName($displayName,$inclusive=false) {
		$table = static::getTable();
		if ($inclusive) {
			$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE display_name = :new AND display_name <> :current");
			$handle->execute([':new'=>$displayName,':current'=>$this->display_name]);
		} else {
			$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE display_name = :new");
			$handle->execute([':new'=>$displayName]);
		}

		if ($handle->rowCount() > 0) {
			return false;
		} else {
			$this->display_name = $displayName;
		}
		return true;
	}

	public function setEmail($email) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE email = ?");
		$handle->execute([$email]);
		if ($handle->rowCount() > 0) {
			return false;
		} else {
			$this->email = $email;
			return true;
		}
	}

	public function setVerified($toggle=true) {
		$this->is_verified = $toggle;
		$this->verification_code = $this->genRandomString();
		return true;
	}

	protected function matchPassword($in_password) {
		return ($this->hashData( $this->user_salt.$in_password ) == $this->password);
	}

	protected function genRandomString($length = 50) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = '';
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters) -1)];
		}
		return $string;
	}

	protected function hashData($data) {
		return hash_hmac('sha512',$data, \SITEKEY );
	}

}
?>
