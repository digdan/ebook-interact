<?php
	class model_adminlog extends ORM {
		var $activity_type;
		var $activity_data;
		var $created;
		var $viewed;

		var $data; //Built data

		const TYPE_SUPPORT=1;
		const TYPE_SALE=2;
		const TYPE_BOOK_REVIEW=3;

		function __construct($id=NULL) {
			$this->created = time();
			$new = parent::__construct($id);
			$new->build();
			return $new;
		}

		function build() { //Builds textual info based on data,
			$this->data = json_decode($this->activity_data);
			switch($this->activity_type) {
				case TYPE_SUPPORT :
					$this->data['user'] = new model_users($this->data['user']);
					break;
				case TYPE_SALE :
					$this->data['book'] = new model_books($this->data['book']);
					$this->data['author'] = new model_users($this->book['author']);
					break;
				case TYPE_BOOK_REVIEW :
					$this->data['book'] = new model_books($this->data['book']);
					break;
			}

		}

		public static function gather_stats() {
			$path_table = ORM::gettable('model_admin_log');
			$handle = DB::instance()->prepare("SELECT activity_type,count(activity_type) as cnt FROM {$path_table} WHERE viewed = 0 GROUP BY activity_type LIMIT 100");
			$handle->execute();
			$out = array(
				self::TYPE_BOOK_REVIEW=>"0",
				self::TYPE_SUPPORT=>"0",
				self::TYPE_SALE=>"0"
			);
			while($res = $handle->fetch()) {
				$out[$res['activity_type']] = $res['cnt'];
			}
			return $out;
		}

		public static function gather() {
			$path_table = ORM::gettable('model_admin_log');
			$handle = DB::instance()->prepare("SELECT id FROM {$path_table} ORDER by created desc LIMIT 100");
			$handle->execute();
			$out = array();
			while($res = $handle->fetch()) {
				$out[] = new self($res['id']);
			}
			return $out;
		}

	}
?>