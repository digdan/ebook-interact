<?php
class model_styles extends ORM {
	//Less/CSS records
	var $css = NULL; //Rendered CSS
	var $less; //Source Less
	var $parent_id;
	var $type; //see Constants

	const TYPE_PREFAB = 1; //parent_id=0;
	const TYPE_BOOK = 2; //parent_id=book_id;
	const TYPE_CHAPTER = 3; //parent_id=chapter_id
	const TYPE_NODE = 4; //parent_id=node_id


	function render($less = NULL) { //LESS into CSS
		$lessc = new lessc;
		if (is_null($less)) return $this->css = $less->compaile($this->less);
		return $less->compile($less);
	}

	function css() {
		if (is_null($this->css)) $this->render();
		return $this->css;
	}

	function setParent($parent_id = NULL, $type = self::TYPE_NODE) {
		$this->parent_id = $parent_id;
		$this->type = $type;
		return $this->save();
	}

}
?>