<?php
class model_chapters extends ORM {
	var $order;
	var $name;
	var $book_id;
	var $node_start_id;
	var $created;

	function __construct($id=NULL) {
		$this->created = time();
		return parent::__construct($id);
	}

	static function nextOrder( $book_id ) {
		$table = static::getTable();
		$handle = DB::instance()->prepare( "SELECT max(`order`) as max_order FROM {$table} WHERE book_id = ?" );
		$handle->execute(array($book_id));
		$res = $handle->fetch();
		return $res['max_order'] + 1;
	}

	static function chapterInBook($book_id,$chapter_id) {
		return (in_array($chapter_id,self::chaptersFromBook($book_id,true)));
	}

	function start_node_id() {
		return $this->node_start_id ;
	}

	function normalize_node_positions() {
		//Load all nodes in chapter by their position order
		//Re number each as its position.
	}

	static function hasStartNode($start_node_id) { //Iterate through each chapter, return those with this as startingn node.
		$table = static::getTable();
		$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE node_start_id = ? ORDER BY `order` ASC");
		$handle->execute(array($start_node_id));
		if ($res = $handle->fetch()) return $res['id'];
		return false;
	}

	static function chaptersFromBook($book_id,$ids_only=FALSE) {
		$table = static::getTable();
		if ($ids_only) {
			$ids = array();
			$handle = DB::instance()->prepare("SELECT id FROM {$table} WHERE book_id = ? ORDER BY `order` ASC");
			$handle->execute(array($book_id));
			if ($handle->rowCount() > 0) {
				while($res = $handle->fetch()) {
					$ids[] = $res["id"];
				}
			}
			return $ids;
		} else {
			$chapters = array();
			$nodeTable = static::getTable('model_nodes');
			$handle = DB::instance()->prepare("SELECT {$table}.*,{$nodeTable}.name as start_node_name from {$table},{$nodeTable} WHERE {$table}.book_id = ? AND {$table}.node_start_id = {$nodeTable}.id ORDER BY `order` ASC");
			$handle->execute(array($book_id));
			while(	$res = $handle->fetch() ) {
				$res["node_count"] = model_chapters::node_count($res["id"]);
				$chapters[] = $res;
			}
			return $chapters;
		}
	}

	function nodesFromChapter( $idOnly=FALSE,$first=false ) {
		$table = static::getTable('model_nodes');
		$nodes = array();
		$limit = ($first?'LIMIT 1':'');
		$handle = DB::instance()->prepare("SELECT id from {$table} WHERE chapter_id = ? ORDER BY position ASC {$limit}");
		$handle->execute(array( $this->id ));
		while(	$res = $handle->fetch() ) {
			if ($idOnly === TRUE) {
				if ($first) return $res['id'];
				$nodes[] = $res["id"];
			} else {
				if ($first) return new model_nodes( $res['id'] );
				$nodes[$res['id']] = new model_nodes( $res["id"] );
			}
		}
		return $nodes;
	}

	function setOrder($order=NULL) {
		$table = static::getTable();
		$handle = DB::instance()->prepare("UPDATE {$table} SET `order` = :order WHERE id = :id");
		$handle->execute(
			array(':order'=>$order,':id'=>$this->id)
		);
	}


	function node_add($name=NULL) {
		$node = new model_nodes();
		$node->chapter_id = $this->id;
		$node->name = (is_null($name)?'Unnamed':$name);
		$node->book_id = $this->book_id;
		$node->save();
		return $node->id;
	}

	static function node_count( $id ) {
		$table = static::getTable('model_nodes');
		$handle = DB::instance()->prepare("SELECT count(id) FROM {$table} WHERE chapter_id = ?");
		$handle->execute(array( $id ));
		$res = $handle->fetch();
		return $res[0];
	}

	static function chapterAdd( $book_id, $name ) {
		$chapter = new self();
		$chapter->book_id = $book_id;
		$chapter->name = (is_null($name) ? 'Unnamed' : $name);
		$chapter->order = self::nextOrder( $book_id );
		$chapter->node_start_id = "0";
		$chapter->save();
		$start_node = model_nodes::create($book_id,$chapter->id,'Starting Node');
		$chapter->node_start_id = $start_node->id;

		$chapter->save();

		return true;
	}

}
?>