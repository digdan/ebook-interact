<?php
	return array(

		'test'=>array('GET|POST','/test','controllers_Test#test'),
		'install'=>array('GET|POST','/install','controllers_Install#install'),

		'front'=>array('GET|POST','/','controllers_Static#display'),
		'tropes'=>array('GET|POST','/tropes','controllers_Tropes#display'),
		'session_dump'=>array("GET|POST",'/session_dump','controllers_Static#session_dump'),

		//User related
		'user/register'=>array('GET|POST','/register','controllers_Register#display'),
		'user/register_verify_sent'=>array('GET|POST','/verify','controllers_Static#display'),
		'user/register_verify'=>array('GET|POST','/verify/[i:user_id]/[*:code]','controllers_Register#verify'),
		'user/login'=>array('GET|POST','/login','controllers_Login#display'),
		'user/logout'=>array('GET|POST','/logout','controllers_Login#logout'),
		'user/settings'=>array('GET|POST','/settings','controllers_Settings#main'),
		'user/lostpassword'=>array('GET|POST','/lostPassword','controllers_Login#lostPassword'),
		'user/lostpassword_verify'=>array('GET|POST','/lostPassword/[i:user_id]/[*:code]','controllers_Login#lostPasswordVerify'),

		
		//Book editor related
		'books/nodes'=>array('GET|POST','/my-books/nodes/[*:id]','controllers_MyBooks#edit'),
		'books/media'=>array('GET|POST','/my-books/media/[*:id]','controllers_MyMedia#editMedia'),
		'books/style'=>array('GET|POST','/my-books/style/[*:id]','controllers_MyBooks#edit'),
		'books/download'=>array('GET|POST','/my-books/download/[*:id]','controllers_MyBook#download'),
		'books/preview'=>array('GET|POST','/my-books/preview/[*:id]','controllers_MyBooks#preview'),
		'books/ipreview'=>array('GET|POST','/my-books/ipreview/[*:id]','controllers_MyBooks#ipreview'),
		'books/ipreview_link'=>array('GET|POST','/my-books/ipreview/[*:id]/[*:cid]','controllers_MyBooks#ipreview'),
		'books/main'=>array('GET|POST','/my-books','controllers_MyBooks#main'),
		'books/edit'=>array('GET|POST','/my-books/edit/[*:id]','controllers_MyBooks#edit'),
		'books/download'=>array('GET|POST|HEAD','/my-books/download/[*:id].epub','controllers_MyBooks#download'),
		'books/delete'=>array('GET|POST','/my-books/delete/[*:id]','controllers_MyBooks#delete'),
		'books/cover'=>array('GET|POST','/cover-[i:width]/[*:id].jpg','controllers_MyBooks#cover'),
		'books/cover_upload'=>array('GET|POST','/cover_upload/[*:id]','controllers_MyBooks#cover_upload'),
		'books/cover_current'=>array('GET|POST','/cover_current-[i:width].jpg','controllers_MyBooks#cover_current'),
		'books/read'=>array('GET|POST','/read/[*:id]','controllers_MyBooks#read'),
		'books/chapters'=>array('GET|POST','/my-books/chapters/[*:id]','controllers_MyChapters#editChapters'),
		'books/chapter'=>array('GET|POST','/my-books/chapter/[*:id]','controllers_MyChapters#editChapter'),
		'books/chapterOrder'=>array('GET|POST','/chapterOrder/[*:book_id]','controllers_MyChapters#chapterOrder'),
		'books/nodes'=>array('GET|POST','/my-books/nodes/[*:id]','controllers_MyNodes#editNodes'),
		'books/node'=>array('GET|POST','/my-books/node/[*:id]/[*:node_id]','controllers_MyNodes#editNode'),
		'books/node-new'=>array('GET|POST','/my-books/new-node/[*:id]/[*:chapter_id]','controllers_MyNodes#newNode'),
		'books/node-reorder'=>array('GET|POST','/my-books/[*:id]/reorder','controllers_MyNodes#nodeReorder'),

		//Monocle epub previewer
		'mono/getComponents'=>array('GET|POST','/mono/getComponents/[i:bid]','controllers_Monocle#getComponents'),
		'mono/getContents'=>array('GET|POST','/mono/getContents/[i:bid]','controllers_Monocle#getContents'),
		'mono/getComponent'=>array('GET|POST','/mono/getComponent/[i:bid]/[*:cid]','controllers_Monocle#getComponent'),
		'mono/getMetaData'=>array('GET|POST','/mono/getMetaData/[i:bid]','controllers_Monocle#getMetaData'),


		/***** ADMIN ROUTES **********/
		'admin'=>array("GET|POST",'/admin','controllers_Admin#main'),
		'admin/review'=>array("GET|POST",'/admin-review','controllers_Admin#review'),
		'admin/sales'=>array("GET|POST",'/admin-review','controllers_Admin#sales'),
		'admin/support'=>array("GET|POST",'/admin-review','controllers_Admin#support'),
	);
?>
