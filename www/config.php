<?php
/**
 * Config File to be used with Config Singleton
 *
 * Makes for highly portable configuration file
 *
 */
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
define('DEVEL',TRUE);
define('SITEKEY','abc123');
 return array(
 	'meta'=>array(
		'title'=>'Ebook Interact',
		'keywords'=>'Ebook, Interact',
		'author'=>'Danjelo Morgaux',
		'description'=>'Non-linear Stories'
	),
	'site'=>array(
		'key'=>'E4qxUmmfEduUie4tNNUWvhNtuVCNEbtMCart'
	),
	'books_directory'=>dirname(APP_ROOT)."/books",
	'cookie_domain'=>'.ebookinteractive.com',
	'cache'=>array(
		'host'=>'localhost',
		'port'=>11211
	),
	'paths'=>array(
		'templates'=> __DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR,
	),
	'db'=>array(
		'dsn'=>'mysql:host=localhost;dbname=ebi',
		'username'=>'ebi',
		'password'=>'aGzvLWwt4yVKApYE'
	),
	'session'=>array(
		'remember_time'=> 60 * 60 * 48 //
	),
	'avatar'=>array(
		'max_width'=>300,
		'max_height'=>300
	),
	'register'=>array(
		'verify_email_template'=>'emails/verify_email',
		'verify_email_subject'=>'Welcome to EBI',
		'verify_email_sender'=>'Ebook Interact',
		'verify_email_sender_address'=>'info@ebookinteract.com',
		'lostpassword_email_template'=>'emails/lostpassword_email',
		'lostpassword_email_subject'=>'Reset lost password on EBI',
	)
 );
?>
