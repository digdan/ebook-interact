<?php

/**
 * Class Scope
 *
 * 	Scope is a variable container that is used to transfer data to a presentation layer.
 *
 */
class Scope {

	var $payload;
	var $basePath;
	var $route;

	static $instances;
	static $current_instance='default';
	static $preBase;

	static function setBase($base) {
		static::$preBase = $base;
	}

	static function instance($name=NULL, $basePath=NULL ) {
		if (is_null($name)) {
			$name = self::$current_instance;
		} else {
			self::$current_instance = $name;
		}
		if ( ! isset(self::$instances[$name])) {
			if (is_null($basePath) and isset(self::$preBase)) {
				$basePath = self::$preBase;
			}
			self::$instances[$name] = new self( $basePath );
		}
		return self::$instances[$name];
	}

	function __construct( $basePath=NULL ) {
		if (!is_null($basePath)) {
			$this->basePath = $basePath;
		}
	}

	public function inject($invar,$value=NULL) {
		if (is_array($invar)) {
			foreach($invar as $k=>$v) {
				$this->payload[$k] = $v;
			}
		} else {
			if ( ! is_null($value)) {
				$this->payload[$invar] = $value;
			} else {
				$this->payload[] = $invar;
			}
		}
		return $this;
	}

	public function can_render($template) {
		$target_file = $this->basePath.$template.".php";
		return file_exists($target_file);
	}

	public function render( $template=NULL, $vars=NULL ) {
		$this->route = $template;
		if (is_null($template)) { //Ajax,REST
			return json_encode($vars);
		} else { //HTML
			ob_start();
			extract($this->payload);
			if (!is_null($vars)) extract($vars);
			if ( ! defined('TEMPLATE_PATH') ) define('TEMPLATE_PATH',$this->basePath);
			$target_file = $this->basePath.$template.".php";
			if (file_exists($target_file)) {
				chdir(TEMPLATE_PATH);
				include( $target_file );
			} else {
				$route = "404";
				include( $this->basePath."404.php" );
			}
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}

	}

}
?>