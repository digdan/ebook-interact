<?php
	class Session {

		function isAuth() {
			if (isset($_SESSION["user_id"])) {
				return true;
			} return false;
		}

		static function mustAuth() {
			if (isset($_SESSION["user_id"])) return true;
			header("Location: ".Router::instance()->generate('user/login'));
			die();
		}

		static function user_id() {
			return (isset($_SESSION["user_id"]) ? $_SESSION["user_id"] : NULL );
		}

	}
?>