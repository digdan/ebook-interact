<?php
namespace Epub;
interface iOCF {
	function render(); //Render OCF file
}
class OCF implements iOCF {
	function render() {
		return file_get_contents(__DIR__.'/templates/container.xml');
	}
}