<?php
namespace Epub;

interface iOPF {
	function dcmi($key,$value); //Add DMCI info
	function meta($key,$value); //Add Meta Data info
	function manifest($name,$path,$mime); //Add file to manifest
	function spine($name,$properties); //Add file to reading sequence
	function render(); //Build and displays OPF file
}

class OPF implements iOPF {
	var $meta; //Meta Data
	var $dcmi; //DCMI Specific Meta Data
	var $manifest; //Manifest of documents
	var $spine; //Reading Order

	function __construct() {
		$this->meta = $this->dcmi = $this->manifest = $this->spine = array();
	}

	function dcmi($key,$value=NULL) {
		if (is_null($value)) return $this->dcmi[$key];
		$this->dcmi[$key] = $value;
	}

	function meta($key,$value=NULL) {
		if (is_null($value)) return $this->meta[$key];
		$this->meta[$key] = $value;
	}

	function manifest($name,$path,$mime) {
		$this->manifest[] = array('name'=>$name,'path'=>$path,'mime'=>$mime);
	}

	function spine($name,$properties=NULL) {

		$default = array(
			'linear'=>false
		);
		$props = ( ! is_null($properties) ) ? array_merge($default,$properties) : $default;

		$linear = $props['linear'];
		unset($props['linear']);
		if (count($props) > 0) {
			foreach($props as $k=>$v) {
				$extras[] = "{$k}:{$v}";
			}
			$extra = join(",",$extras);
		} else {
			$extra = NULL;
		}

		$this->spine[] = array('name'=>$name,"linear"=>$linear,"extra"=>$extra);

	}

	function render() {
		$dcmi = '';
		foreach($this->dcmi as $dcmi_key=>$dcmi_value) {
			$dcmi .= "<dc:{$dcmi_key} id=\"{$dcmi_key}\">{$dcmi_value}</dc:{$dcmi_key}>\n";
		}

		$meta = '';
		foreach($this->meta as $meta_key=>$meta_value) {
			$meta .= "<meta property=\"{$meta_key}\">{$meta_value}</meta>\n";
		}

		$manifest = '';
		foreach($this->manifest as $manifest_file) {
			$path = str_replace("OPS/","",$manifest_file['path']); //Remove local relative OPS pathing
			$manifest .= "<item id=\"{$manifest_file['name']}\" href=\"{$path}\" media-type=\"{$manifest_file['mime']}\"/>\n";
		}

		$spine = '';
		foreach($this->spine as $spine_value) {
			$spine .= "<itemref idref=\"{$spine_value['name']}\" linear=\"";
			$spine .= ($spine_value['linear'] ? 'yes' : 'no' );
			$spine .= "\" ";
			if (!is_null($spine_value['extra'])) {
				$spine .= "properties=\"{$spine_value['extra']}\" ";
			}
			$spine .= "/>\n";
		}

		ob_start();                      // start capturing output
		include('templates/package.opf');   // execute the file
		$content = ob_get_contents();    // get the contents from the buffer
		ob_end_clean();
		return $content;
	}
}
?>