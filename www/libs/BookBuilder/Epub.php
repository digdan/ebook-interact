<?php
	include_once("OPF.php");
	include_once("OCF.php");
	include_once("Filters.php");

	interface iEpub {
		function opf(Epub\iOPF $opf);
		function ocf(Epub\iOCF $ocf);
		function zip(zipArchive $zip);
		function dcmi($key,$value); //Add DCMI information to epub
		function meta($key,$value); //Add MetaData to epub
		function spine($id,$properties); //Add page to linear book sequence
		function addFile($location,$local_location,$id); //Add a local/remote file to epub
		function addFileFromString($string,$local_location,$id); //Add a file based on a string
		function addFileFromRender($render,$local_location,$id); //Add a compiled file to epub.

		function render($obj_type); //Render a meta file
		function save($file); //Save Epub to file
	}

	class EPub implements iEpub {
		//Basic Zip file with mimetype base64 encoded
		const ZIP_STUB='UEsDBAoAAAAAAHkBVUVvYassFAAAABQAAAAIAAAAbWltZXR5cGVhcHBsaWNhdGlvbi9lcHViK3ppcFBLAQIeAwoAAAAAAHkBVUVvYassFAAAABQAAAAIAAAAAAAAAAAAAACkgQAAAABtaW1ldHlwZVBLBQYAAAAAAQABADYAAAA6AAAAAAA=';
		const OCF=1;
		const OPF=2;

		protected $opf; //Open Package Format
		protected $ocf; //Open Container Format
		protected $zip; //ZipArchive Obj

		//Strategy design. Subject images/css/html to numerous "filters" before rendering final results.
		protected $imageFilters;
		protected $cssFilters;
		protected $htmlFilters;

		var $ids; //List of asset IDs in Epub
		var $filename; //Working zipFile

		function __construct() {
			//Setup Sub Objects (Facade)
			$this->opf( new Epub\OPF() );
			$this->ocf( new Epub\OCF() );

			//Setup filters
			$this->imageFilters = new Epub\FilterContainer();

			$this->cssFilters = new Epub\FilterContainer();
			$this->cssFilters->addFilter( new Epub\Render_CSSFilter );

			$this->htmlFilters = new Epub\FilterContainer();

			if ($this->zip( new ZipArchive() )) {
				if ($this->zip->open($this->buildStub()) === TRUE) {
					$this->zip->addEmptyDir('META-INF');
					$this->zip->addEmptyDir('OPS');
				} else return false;
			} else return false;
		}

		private function buildStub() { //Bulid basic zip file from encoded class const
			$this->filename = tempnam('/tmp','ebi');
			$fp = fopen($this->filename,"wb");
			fputs($fp,base64_decode(self::ZIP_STUB));
			fclose($fp);
			return $this->filename;
		}

		public function opf(Epub\iOPF $opf) { $this->opf = $opf; return ;}
		public function ocf(Epub\iOCF $ocf) { $this->ocf = $ocf; return true;}
		public function zip(zipArchive $zip) { $this->zip = $zip; return true;}
		public function dcmi($key,$value=NULL) { return $this->opf->dcmi($key,$value); } //Facade to OPF dcmi
		public function meta($key,$value=NULL) { return $this->opf->meta($key,$value); } //Facade to OPF meta
		public function spine($id,$properties=NULL) { return $this->opf->spine($id,$properties); } //Facade to OPF spine

		public function addFile($location,$local_filename,$id=NULL) {

			$filename = NULL;
			if(filter_var($location, FILTER_VALIDATE_URL)) {
				$content = file_get_contents( $location );
			} else {
				if (file_exists($location)) {
					$content  = file_get_contents($location);
				} else {
					echo "File not found : {$location}.\n";
					return false; //Error
				}
			}
			return $this->addFileFromString($content,$local_filename,$id);
		}

		public function addFileFromRender($render,$local_file,$id=NULL) { //
			$content = $this->render($render);
			return $this->addFileFromString($content,$local_file,$id);
		}

		public function addFileFromString($string,$local_filename='',$id=NULL) {
			if ($local_filename == '') return false;
			//Find file type
			$fi = new finfo(FILEINFO_MIME_TYPE);
			$mime_type = $fi->buffer($string);
			$ext = pathinfo($local_filename, PATHINFO_EXTENSION);

			switch($ext) {
				case "css" :
				case "less" :
					$mime_type = 'text/less';
					break;
			}

			switch($mime_type) {
				case 'image/jpeg':
				case 'image/png':
				case 'image/gif':
					$string = $this->imageFilters->run($string); break;

				case 'text/css':
				case 'text/less':
					$string = $this->cssFilters->run($string); break;
				default:break;
			}

			$id = Utils::unique_id( $id, $this->ids ); //Make sure ID is unique

			if ( $id != false ) $this->opf->manifest($id,$local_filename,$mime_type); //Add file to manifest

			$this->zip->addFromString($local_filename,$string);
			return $id;
		}

		public function render($obj_type=NULL) { //Renders components of Epub
			switch($obj_type) {
				case EPUB::OCF : return $this->ocf->render(); break;
				case EPUB::OPF : return $this->opf->render(); break;
			}
		}

		public function xhtml($scope=array()) {
			//Todo - Add header, CSS, JS, viewport, meta, the whole bit
			$css = (isset($scope['css'])?$scope['css']:array());
			$content = (isset($scope['content'])?$scope['content']:'');
			$title = (isset($scope['title'])?$scope['title']:'No Title');
			$navigation = (isset($scope['navigation'])?$scope['navigation']:'');
			ob_start();
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			include('templates/template.xhtml');
			$out = ob_get_contents();
			ob_end_clean();
			return $out;
		}

		private function saveFKeeper(FKeeper $fkeep) {
			$this->zip->close();
			$hash = $fkeep->keep($this->filename);
			return $hash;
		}

		public function save($local_file) {
			if ($local_file instanceof FKeeper) { //Save via FKeeper
				return $this->saveFKeeper($local_file);
			} else { //Save to file
				$this->zip->close();
				return rename($this->filename,$local_file);
			}
		}
	}
?>