<?php
	namespace Epub;

	interface iFilter {
		function filter($subject);
	}

	interface iFilterContainer {
		function addFilter(iFilter $filter);
	}

	abstract class Filter implements iFilter {
		function filter($subject) {
			return $subject;
		}
	}

	class FilterContainer implements iFilterContainer {
		var $filters;
		var $containerType=0;
		function addFilter( iFilter $filter ) {
			$this->filters[] = $filter;
		}
		public function run( $subject ) {
			if (isset($this->filters) and (count($this->filters) > 0)) {
				foreach ($this->filters as $filter) {
					$subject = $filter->filter($subject);
				}
			}
			return $subject;
		}
	}

	interface iImageFilter extends iFilter {
	}

	interface iCSSFilter extends iFilter {
	}

	interface iHTMLFilter extends iFilter {
	}


	class Render_CSSFilter extends Filter implements iCSSFilter { //Renders LESS into CSS
		function filter($subject) {
			$less = new \lessc;
			return $less->compile($subject);
		}
	}

?>