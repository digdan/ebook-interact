<?php
	include_once(APP_ROOT."/libs/BookBuilder/Epub.php");

/**
 * Class BookBuilder
 *
 * $builder = BookBuilder::generateBook( $book->id );
 * $FKeeperHash = $builder->render();
 */
	class BookBuilder { //Singleton for generating books
		static $instance;

		var $filename;
		var $assets;

		var $book;
		var $epub;

		protected $misc;

		public function __construct( model_books $book ) {
			$this->epub = new Epub();
			$this->book = $book;

			$this->buildMeta();
			$this->buildCSS();
			$this->buildCover();
			$this->buildMisc();
			$this->buildChapters();
		}

		public function render() {  //Finalize and save ebook.
			$this->epub->addFileFromRender( EPUB::OCF , 'META-INF/container.xml', FALSE );
			$this->epub->addFileFromRender( EPUB::OPF , 'OPS/package.opf', FALSE );

			$hash = $this->epub->save( FKeeper::instance() ); //Save to FKeeper

			//CONVERT to MOBI
			return $hash;
		}

		private function buildChapters() {
			$chapter_ids = model_chapters::chaptersFromBook($this->book->id,TRUE);
			$css = array( $this->assets['css']['system'] );
			foreach( $chapter_ids as $chapter_id) {
				$chapters[$chapter_id] = new model_chapters( $chapter_id );
				$chapter_filenames[ $chapter_id ] = 'chapter_'.$chapter_id;
			}
			foreach( $chapters as $chapter) {
				$node_pages = array();
				$nodes = $chapter->nodesFromChapter();
				$start_node = $nodes[ $chapter->node_start_id ];
				unset( $nodes[ $chapter->node_start_id ] );
				foreach($nodes as $node) $filenames[ $node->id ] = 'node_'.$node->id;
				$filenames[$start_node->id] = $chapter_filenames[$chapter->id]; //Start node takes chapters ID
				array_unshift( $nodes, $start_node ); //Extract starting node and place in front

				//Build Node Path Content
				//Todo node templating ( using Rain )
				foreach($nodes as $node) $node_navigation[$node->id] =  model_paths::renderPaths( $node ) ;

				foreach($nodes as $node) 
					$this->epub->spine( $node_pages[ $node->id ] = $this->epub->addFileFromString( $this->epub->xhtml( array('title'=>$node->name, 'navigation'=>$node_navigation[$node->id],'content'=>$node->content, 'css'=>$css ) ) ,'OPS/'.$filenames[ $node->id ].'.xhtml') , array('linear'=>'no') );
			}
		}

		private function buildCSS() {
			//Load all style assets, compile them, and link them to their filenames.
			$this->assets['css'] = array('system'=>'system.css');
			$this->epub->addFileFromString( file_get_contents(__DIR__.'/templates/system.less') ,'OPS/'.$this->assets['css']['system']);

		}

		private function buildMisc() {
			//About the Author

			//Preface

			//Game Rules ~ This is not a linear book explaination

		}
		private function buildCover() {
			$cover_image = $this->epub->addFile( Router::instance()->url_prefix().Router::instance()->generate("books/cover",array('id'=>$this->book->id,'width'=>500)), "OPS/images/cover.jpg");
			$cover_page = $this->epub->addFileFromString($this->epub->xhtml(array('title'=>'Cover Page','content'=>file_get_contents('libs/BookBuilder/templates/cover.xhtml'))),'OPS/cover.xhtml');
			$this->misc['cover'] = $cover_page;
			$this->epub->spine( $cover_page , array('linear'=>TRUE,'rendition'=>'page-spread-center') );
		}

		private function buildMeta() {
			//META
			$this->epub->dcmi('title',$this->book->title);
			$this->epub->dcmi('creator',$this->book->getAuthor()->display_name);
			switch($this->book->language) {
				default :
					$this->epub->dcmi('language','en');
					break;
			}
			$this->epub->dcmi('identifier','EBI-'.$this->book->id);
			$this->epub->dcmi('source','http://www.ebookinteractive.com');
			$this->epub->dcmi('date',date("l"));
			$this->epub->dcmi('publisher','Ebook Interactive');
			$this->epub->dcmi('rights','Copyright '.date('Y').' Ebook Interactive All rights reserved.');
			$this->epub->dcmi('description',$this->book->description);

			$this->epub->meta('rendition:layout','pre-paginated');
			$this->epub->meta('rendition:orientation','landscaped');
			$this->epub->meta('rendition:spread','none');
			$this->epub->meta('dcterms:modified','2013-04-11T04:10:23Z');
		}

		public function get() {
			if ( isset(self::$instance) ) return self::$instance;
			return false;
		}
		static function generateBook( $book_id ) {
			$book = new model_books( $book_id );
			self::$instance = new self( $book );
			return self::$instance;
		}

	}
?>
