<?php

/**
 * Class workTable
 *
 * 		Creates a temporary working directory
 */
	class workTable {
		public $base=NULL;
		public $hash=NULL;
		static $instances;
		static $current_instance='default';

		static function instance($name=NULL,$base=NULL) {
			if (is_null($name)) {
				$name = self::$current_instance;
			} else {
				self::$current_instance = $name;
			}

			if ( ! isset( self::$instances[$name] )) {
				self::$instances[$name] = new self($base);
			}

			return self::$instances[$name];
		}

		function getTable() {
			$this->hash = substr(md5(uniqid(rand(0,9999),true)),0,8);
			$path[0] = $this->base;
			$path[1] = substr($this->hash,0,2);
			$path[2] = substr($this->hash,2,2);
			$path[3] = substr($this->hash,4);
			$path = join("/",$path);
			if (is_dir($path)) {
				unlink($path);
			}
			mkdir($path);
			return $path;
		}


		function __construct($base=NULL) {
			//Does base path exist?
			if ( ! is_null($base)) {
				if (is_dir($base)) {
					$this->base = $base;
				} else {
					die('Unable to find workTable Base : '.$base);
					return false;
				}
			} else return false;
		}

	}
?>