<?php

/**
 * Class DB
 *
 * DB Singleton class that extends PDO
 */
class DB extends PDO {
		static $instances;
		static $current_instance='default';

		function __construct($dsn=NULL,$username=NULL,$password=NULL) {
			parent::__construct($dsn,$username,$password);
		}

		public static function instance($name=NULL,$dsn=NULL,$username=NULL,$password=NULL) {
			if (is_null($name)) {
				$name = self::$current_instance;
			} else {
				self::$current_instance=$name;
			}

			if (is_null(self::$instances[$name])) {
				self::$instances[$name] = new self($dsn,$username,$password);
			}
			return self::$instances[$name];
		}
	}
?>