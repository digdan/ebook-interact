<?php
/** FKeeper
 * 	A file system organizer
 */
class FKeeper {
	public $base=NULL;
	public $hash=NULL;
	static $instances;
	static $current_instance='default';

	static function instance($name=NULL,$base=NULL) {
		if (is_null($name)) {
			$name = self::$current_instance;
		} else {
			self::$current_instance = $name;
		}

		if ( ! isset( self::$instances[$name] )) {
			self::$instances[$name] = new self($base);
		}

		return self::$instances[$name];
	}

	function __construct($base=NULL) {
		//Does base path exist?
		if ( ! is_null($base)) {
			if (is_dir($base)) {
				$this->base = $base;
			} else {
				die('Unable to find FKeeper Base : '.$base);
				return false;
			}
		} else return false;
	}

	function keep($filename, $isUploaded=false) { //Generates a path and filename to be saved, based on file contents
		if (is_array($filename)) {
			$out = array();
			foreach($filename as $single) $out[] = $this->keep($single,$isUploaded);
			return $out;
		} else {
			if (file_exists($filename) and ($hash = self::hashFile($filename))) {
				$this->hash = $hash;
				$path[0] = $this->base;
				$path[1] = substr($hash,0,2);
				$path[2] = substr($hash,2,2);
				$path[3] = substr($hash,4);
				if ($this->dirMakeExist( join("/",array($path[0],$path[1])) )) {
					if ($this->dirMakeExist( join("/",array($path[0],$path[1],$path[2])) )) {
						$endPath = join("/",$path);
						if ($isUploaded === TRUE) {
							move_uploaded_file($filename,$endPath);
						} else {
							rename($filename,$endPath);
						}
						return $this->hash;
					} else return false;
				} else return false;
			} else return false;
		}
	}

	function info($filename) {
		if (file_exists($filename)) {
			$info = new SplFileInfo($filename);
			$out = array(
				'ext'=>$info->getExtension(),
				'atime'=>$info->getATime(),
				'basename'=>$info->getBasename(),
				'ctime'=>$info->getCTime(),
				'size'=>$info->getSize(),
				'path'=>$info->getPath(),
				'type'=>$info->getType()
			);
			return $out;
		} else return false;
	}

	static function hashFile($filename) {
		if (file_exists($filename))
			return sha1_file($filename);
		return false;
	}

	function hashPath($hash) {
		$path[0] = $this->base;
		$path[1] = substr($hash,0,2);
		$path[2] = substr($hash,2,2);
		$path[3] = substr($hash,4);
		return join("/",$path);
	}

	private function dirMakeExist($path) {
		if (! is_dir($path)) {
			if ( ! @mkdir($path) ) return false;
			return true;
		}
		return true;
	}

	function delete($hash) {
		return unlink(self::hashPath($hash));
	}

}
?>