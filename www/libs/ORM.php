<?php

/**
 * Class ORM
 *
 * ORM is an abstract class that is extended by objects that mimic a database table.
 * The ORM is tied to DB.
 */
abstract class ORM {
	public $id=NULL;
	var $created;

	static $structures=array();

	function __construct($id=NULL) {
		if ( ! is_null($id) ) {
			return $this->loadFromID($id);
		}
	}

	static function instance($id=NULL) {
		return new static($id);
	}

	public function loadFromID($id=NULL) {
		$table = self::getTable();
		$handle = DB::instance()->prepare("SELECT * FROM {$table} WHERE id = :id");
		$handle->execute(array(":id"=>$id));
		$handle->setFetchMode(\PDO::FETCH_INTO, $this );
		return $handle->fetch();
	}

	public function save( $allowedFields=NULL) {
		$table = static::getTable();
		if ( ! isset(self::$structures[$table])) {
			$sql = "DESCRIBE `".$table."`";
			self::$structures[$table] = DB::instance()->query($sql)->fetchAll();
		}

		$vars = get_object_vars($this);
		if ( ! is_null($allowedFields)) $vars = array_intersect($vars,$allowedFields);
		$params = $head = $values = array();

		foreach(self::$structures[$table] as $field) {
			if ($field["Field"] != "id") {
				if (array_key_exists($field["Field"],$vars)) {
					$head[$field["Field"]] = "`".$field["Field"]."`";
					$values[$field["Field"]] = ':'.$field["Field"];
					$update[] = "`".$field["Field"]."` = :".$field["Field"];
					$params[':'.$field["Field"]] = $this->$field["Field"];
				}
			}
		}

		if (is_null($this->id)) { //Insert
			$sql = "INSERT INTO {$table} (".join(",",$head).") VALUES (".join(",",$values).")";

			$handle = DB::instance()->prepare($sql);
			if ($handle->execute($params)) {
				$this->id = DB::instance()->lastInsertId();
			} else {
				if (defined('DEVEL') && (DEVEL === TRUE)) var_dump($handle->errorInfo());
				return false;
			}
			return $this->id;
		} else { //Update
			$params[':id'] = $this->id;
			$sql = "UPDATE {$table} SET ".join(",",$update)." WHERE id = :id";
			$handle = DB::instance()->prepare($sql);
			if ($handle->execute($params)) {
				return true;
			} else {
				if (defined('DEVEL') && (DEVEL === TRUE)) var_dump($handle->errorInfo());
				return false;
			}
		}
	}

	public function delete() {
		if ( ! $this->id ) return false;
		$table = static::getTable();
		$handle = DB::instance()->prepare("DELETE FROM {$table} WHERE id = :id");
		return $handle->execute(array(':id'=>$this->id));
	}

	static function getTable($class=NULL) {
		if (is_null($class)) $class = get_called_class();
		$name_parts = explode("_",$class);
		return array_pop($name_parts);
	}
}
?>