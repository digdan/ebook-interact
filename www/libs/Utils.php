<?php
	class Utils {
		static function slugify($string){
			return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		}

		static function genRandomString($length = 50) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
			$string = '';
			for ($p = 0; $p < $length; $p++) {
				$string .= $characters[mt_rand(0, strlen($characters) -1)];
			}
			return $string;
		}

		static function hashData($data) {
			return hash_hmac('sha512',$data, \SITEKEY );
		}

		public static function unique_id($id=NULL,$id_list=NULL) {
			if (is_null($id)) $id = 'id-'.md5(uniqid('ebi-',true));
			while( isset($id_list[$id]) ) {
				$idp = explode('-',$id);
				if (is_numeric($lid = array_pop($idp))) { //Affix incrementing int to make ID unique
					$pre = join("-",$idp);
					$lid++;
					$id = $pre."-".$lid;
				} else {
					$id .= "-1";
				}
			}
			return $id;
		}

		public static function fileExt($contentType) {
			$map = array(
				'application/x-font-woff'		=> '.woff',
				'application/pdf'   			=> '.pdf',
				'image/svg+xml'					=> '.svg',
				'application/oebps-package+xml'	=> '.opf',
				'application/xhtml+xml'			=> '.xhtml',
				'application/zip'   			=> '.zip',
				'image/gif'         			=> '.gif',
				'image/jpeg'        			=> '.jpg',
				'image/png'         			=> '.png',
				'text/css'          			=> '.css',
				'text/html'         			=> '.html',
				'text/javascript'   			=> '.js',
				'text/plain'        			=> '.txt',
				'text/xml'          			=> '.xml',
			);
			if (isset($map[$contentType])) {
				return $map[$contentType];
			}

			$pieces = explode('/', $contentType);
			return '.' . array_pop($pieces);
		}
	}
?>