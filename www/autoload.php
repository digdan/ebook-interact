<?php
/*
	function ebi_autoload($className) {
		$extensions = array(".php", ".class.php", ".inc");
		$paths = explode(PATH_SEPARATOR, get_include_path());
		$className = str_replace("_" , DIRECTORY_SEPARATOR, $className);
		foreach ($paths as $path) {
			$filename = $path . DIRECTORY_SEPARATOR . $className;
			$dirname = $path . DIRECTORY_SEPARATOR . $className. DIRECTORY_SEPARATOR . $className;
			foreach ($extensions as $ext) {
				if (is_readable($filename . $ext)) {
						require_once $filename . $ext;
						break;
				}
				if (is_readable($dirname . $ext)) {
					require_once $dirname . $ext;
					break;
				}
			}
		}
	}
*/

	function ebi_autoload($class) {
		$class = ltrim($class,'\\');
		$class = ($class?strtr($class,'\\','/'):$class);
		$className = str_replace("_","/",$class);
		foreach( explode(':', get_include_path()) as $path) {
			$targets[0] = $path.'/'.$class.'.php';
			$targets[1] = $path.'/'.$className.".php";
			//$targets[2] = $path.'/'.$class.'/'.$class.'.php';
			foreach($targets as $target) {
				if(is_readable($target)) {
					return require_once($target);
				}
			}
		}
	}

	spl_autoload_register('ebi_autoload');
?>
