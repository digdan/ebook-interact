<?php
class controllers_Login {
	function display($params,$route) {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			Scope::instance()->inject('message', $this->post($params,$route));
		}

		if (Scope::instance()->can_render($route)) {
			echo Scope::instance()->render($route);
		} else {
			return false;
		}
	}

	function logout($params,$route) {
		session_destroy();
		setcookie('remember','');
		header("Location: /");
	}

	function post($params,$route) {
		$config = Config::instance();
		$message = "Unable to login";
		switch (model_users::login($_POST["email"],$_POST["password"],@$_POST["remember"])) {
			case model_users::LOGGED_IN :
				$user = model_users::instanceByEmail( $_POST["email"] );
				if ($online = model_online::instanceByUserID( $user->id )) $online->delete();
				$online = new model_online();
				$online->build( $user, (@$_POST['remember'] == 1) );
				if ($online->save()) {
					header("Location: /");
					die();
				}

			break;

			case model_users::PASSWORD_WRONG :
				$message = "Invalid Password";
			break;

			case model_users::NOT_VERIFIED :
				controllers_Register::verify_email( model_users::instanceByEmail($_POST["email"]) );
				$message = "Email not verified. Verification resent";
			break;

			case model_users::NOT_ACTIVE :
				$message = "Account is disabled";
			break;

			case model_users::LOGIN_ERROR :
				$message = "Error logging in";
			break;

			case model_users::NOT_FOUND :
				$message = "Email address not found";
			break;

			default :
				$message = "Unknown error";
			break;
		}
		return $message;
	}

	function lostPassword($params,$route) {
		$config = Config::instance();
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$user = model_users::instanceByEmail( $_POST["email"] );
			$user->resetVerification();
			$user->save();
                        $old = Scope::$current_instance;
                        $mail_scope = Scope::instance('lostpassword_email',$config["paths"]["templates"]);
                        $mail_scope->inject('code',$user->resetVerification());
                        $mail_scope->inject('url',Router::instance()->generate('user/lostpassword_verify',array('user_id'=>$user->id,'code'=>$user->verification_code)));
                        $email = $mail_scope->render($config['register']['lostpassword_email_template']);
                        Scope::instance($old);
                        Mail::instance('lostpassword_email',$user->email,$config['register']['lostpassword_email_subject'],$email)->setFrom($config['register']['verify_email_sender'],$config['register']['verify_email_sender_address'])->send();
			echo Scope::instance()->render('user/lostpassword-sent');
			return true;

		}
		if (Scope::instance()->can_render($route)) {
			echo Scope::instance()->render($route);
			return true;
		} else {
			return false;
		}
	}

	function lostPasswordVerify($params,$route) {
		var_dump($params);
	}
}
?>
