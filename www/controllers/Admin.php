<?php
	class controllers_Admin {
		function main($params,$route) {
			$s = Scope::instance();
			echo $s->render('admin/main');
		}

		function sales($params,$route) {
			$s = Scope::instance();
			echo $s->render('admin/sales');
		}
	
		function support($params,$route) {
			$s = Scope::instance();
			echo $s->render('admin/support');
		}

		function review($params,$route) {
			$s = Scope::instance();
			echo $s->render('admin/review');
		}
	}
?>
