<?php
	class controllers_Static {

		static function display($params,$route) {
			if (Scope::instance()->can_render($route)) {
				echo Scope::instance()->render($route);
			} else {
				return false;
			}
		}

		static function session_dump($params,$route) {
			var_dump($_SESSION);
		}
	}
?>