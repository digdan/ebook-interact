<?php
class controllers_MyMedia {

	function editMedia($params,$route) {
		$book = new model_books($params['id']);

		Scope::instance()->inject("breadcrumb",array(
			"Your Books"=>Router::instance()->generate("books/main"),
			"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
			"Edit Media"=>""
		));

		Scope::instance()->inject("book",$book);
		Scope::instance()->inject("active","media");
		echo Scope::instance()->render('books/edit');
	}
}
?>