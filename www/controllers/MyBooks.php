<?php
class controllers_MyBooks {
	function main($params,$route) {
		if (Scope::instance()->can_render($route)) {
			Session::mustAuth();
			$books = model_books::allBooksByUser($_SESSION["user_id"]);
			Scope::instance()->inject("books",$books);
			echo Scope::instance()->render($route);
		} else {
			return false;
		}
	}

	function delete( $params, $route ) {
		$book = new model_books($params["id"]);
		if ($book->isOwner($_SESSION["user_id"])) {
			$book->delete();
		}

		header("Location: ".Router::instance()->generate("books/main"));
		die();
	}

	function edit($params,$route) {
		Session::mustAuth();
		if (isset($params["id"]) and ($params["id"] > 0)) {
			//Update existing book.
			$book = model_books::instance($params["id"]);
		} else { //New Book
			$book = new model_books();
			$book->user_id = $_SESSION["user_id"];
			$book->cover_image_id = 0;
		}
		if ( ! $book->isOwner( Session::user_id() )) return;
		if ($_SERVER["REQUEST_METHOD"] == "POST") { //POST
			if (isset($_POST['cmd']) and ($_POST['cmd'] == 'Download')) {
				model_ebooks::download($params["id"]);
				die();
			}
			$book->title = $_POST["title"];
			$book->description = $_POST["description"];
			if (isset($_POST["published"])) {
				$book->publish = $_POST["published"];
			} else {
				$book->publish = 'closed';
			}
			$book->language = $_POST["language"];
			$book->license = $_POST["license"];

			if (isset($_SESSION["cover_upload"])) {
				if (isset($book->cover_image_id)) { //Remove previous image
					$image = model_images::instance($book->cover_image_id);
					$image->delete();
				}
				$image = new model_images();
				$obj = Cache::instance()->get($_SESSION["cover_upload"]);
				$image->data = $obj["data"];
				$image->mime = $obj["mime"];
				$image->sizes = serialize($obj["size"]);
				$image->user_id = $_SESSION["user_id"];
				$image->book_id = 0;
				$image->save();
				$book->cover_image_id = $image->id;
			}
			$book->save();
			if (isset($image)) {
				$image->book_id = $book->id;
				$image->save();
			}
			if ($params['id'] == 0) {
				$url = Router::instance()->generate("books/edit",array('id'=>$book->id));
				header("Location: {$url}");
				die();
			}
			if (isset($_POST['cmd']) and ($_POST['cmd'] == 'Build')) {
				$book->build();
			}
		}

		unset($_SESSION["cover_upload"]);
		Scope::instance()->inject("id",$params["id"]);
		if ($params["id"] > 0) {
			Scope::instance()->inject("book",$book);
		} else {
			Scope::instance()->inject("book",$book);
		}

		Scope::instance()->inject("chapters", model_chapters::chaptersFromBook( $params["id"], FALSE ));
		Scope::instance()->inject("breadcrumb",array(
			"Your Books"=>Router::instance()->generate("books/main"),
			"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
		));
		Scope::instance()->inject("active","settings");

		echo Scope::instance()->render('books/edit');
	}

	function preview($params,$route) { //Preview page
		$book = new model_books($params['id']);
		Scope::instance()->inject("breadcrumb",array(
			"Your Books"=>Router::instance()->generate("books/main"),
			"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
		));
		Scope::instance()->inject("book_id", $params['id']);
		Scope::instance()->inject("chapters", model_chapters::chaptersFromBook($book->id,FALSE));
		Scope::instance()->inject("map", $book->componentMap() );
		Scope::instance()->inject("book", $book);
		Scope::instance()->inject("active", "preview");
		echo Scope::instance()->render('books/edit-preview');
	}

	function ipreview($params,$route) { //Actual Preview
		if ($book = new model_books($params['id'])) {
			if (!$book->isOwner()) return false;
			if (isset($params['cid'])) {
				$info = pathinfo($params['cid']);
				$file_name =  basename($params['cid'],'.'.$info['extension']);
				$bparts = explode("_",$file_name);
				$id = $bparts[1];
				switch($bparts[0]) {
					case 'chapter' :
						$chapter = new model_chapters($id);
						$item = new model_nodes( $chapter->node_start_id );
						break;
					case 'node' :
						$item = new model_nodes($id);
						break;
					default :
						$item = false; //Component not found
						break;
				}
				return $item->content();
			} else {
				Scope::instance()->inject("active", "preview");
				Scope::instance()->inject("book", $book);
				Scope::instance()->inject("book_id", $params['id']);
				echo Scope::instance()->render('books/preview');
				die();
			}
		} else 	die('Unable to load book');
	}

	function download($params,$route) {
		var_dump($_SESSION);
		var_dump($_POST);
		die();
		Session::mustAuth();
		$book = new model_books($params['id']);
		if ($book->isOwner($_SESSION["user_id"])) {
			model_ebooks::download($params["id"]);
		} else {
			die('Not your book');
		}
		die();
	}

	function cover_upload($params,$route) {
		$message = "";
		$message_color = "alert-danger";
		if (isset($_FILES["cover"])) {
			if (in_array($_FILES["cover"]["type"],array('image/jpg','image/jpeg','image/png'))) {
				$size = getimagesize($_FILES["cover"]["tmp_name"]);
				list($width,$height) = $size;
				$shortest = ($width > $height ? $height : $width);
				$longest = ($width > $height ? $width : $height);
				if ($shortest < 1000) {
					$message = "Shortest dimension should be greater than 1000 pixels";
					$message_color = 'alert-warning';
				}
				if ($longest > 2500) {
					$message = "Longest dimension should be less than 2500 pixels";
					$message_color = 'alert-warning';
				}
				$obj["name"] = $_FILES["cover"]["name"];
				$obj["mime"] = $_FILES["cover"]["type"];
				$image = new Imagick();
				$max = 2500; //Max width/height of uploaded image
				$image->readImageBlob( file_get_contents($_FILES["cover"]["tmp_name"]) );
				if (($size[0]>$max) || ($size[1]>$max)) {
					if ($size[0] == $longest) {
						$ratio = ($max / $size[0]);
						$image->resizeImage($max,($size[1]*$ratio),Imagick::FILTER_LANCZOS,1);
						$size=array($max,round(($size[1]*$ratio)));
					} else {
						$ratio = ($max / $size[0]);
						$image->resizeImage(($size[0]*$ratio),$max,Imagick::FILTER_LANCZOS,1);
						$size=array(round(($size[0]*$ratio)),$max);
					}
				}
				$obj["size"] = $size;
				$obj["data"] = $image->getImageBlob();
				$object_token = 'cover_upload_'.uniqid(true);
				if (isset($_SESSION["cover_upload"])) { //Remove old cover
					Cache::instance()->delete($_SESSION["cover_upload"]);
				}
				Cache::instance()->set($object_token,$obj,MEMCACHE_COMPRESSED,(time()+(60*60*4)));
				$_SESSION["cover_upload"] = $object_token;
			} else {
				$message = "Unknown file type : {$_FILES["cover"]["type"]}";
			}
		}
		if (isset($params["id"])) {
			$book = new model_books($params["id"]);
		} else {
			$book = new model_books();
		}
		echo Scope::instance()->inject("message",$message)->inject("message_color",$message_color)->inject("book",$book)->render($route);
	}

	function cover_current($params,$route) {
		if (isset($_SESSION["cover_upload"])) {
			$obj = Cache::instance()->get($_SESSION["cover_upload"]);
			$image = new Imagick();
			$image->readImageBlob( $obj["data"] );
			if (isset($params["width"])) { //Dynamically resize cover before displaying
				$ratio = ($params["width"] / $obj["size"][0]);
				$image->resizeImage($params["width"],($obj["size"][1]*$ratio),Imagick::FILTER_LANCZOS,1);
			}
			header("Content-type: {$obj["mime"]}");
			echo $image->getImageBlob();
			die();
		} else {

		}
	}

	function cover($params,$route) {
		if (isset($params["id"]) && ($params["id"] != 0)) {
			//Load image from DB/Cache
			$mimage = model_images::loadByBookCover($params["id"]);
			header('Cache-control: max-age='.(60*60*24*365));
			header('Expires: '.gmdate(DATE_RFC1123,time()+60*60*24*365));
			header('Last-Modified: '.gmdate(DATE_RFC1123,$mimage->created));
			if ($mimage === FALSE) return false;
			header("Content-type: {$mimage->mime}");
			$image = new Imagick();
			$image->readImageBlob( $mimage->data );
			if (isset($params["width"])) { //Dynamically resize cover before displaying
				$size = unserialize($mimage->sizes);
				$ratio = ($params["width"] / $size[0]);
				$image->resizeImage($params["width"],($size[1]*$ratio),Imagick::FILTER_LANCZOS,1);
			}
			echo $image->getImageBlob();
			die();
		} else {
			if (isset($_SESSION["cover_upload"])) {
				$obj = Cache::instance()->get($_SESSION["cover_upload"]);
				$image = new Imagick();
				$image->readImageBlob( $obj["data"] );
				if (isset($params["width"])) { //Dynamically resize cover before displaying
					$ratio = ($params["width"] / $obj["size"][0]);
					$image->resizeImage($params["width"],($obj["size"][1]*$ratio),Imagick::FILTER_LANCZOS,1);
				}
				header("Expires: Sat, 26 Jul 2020 05:00:00 GMT");
				header("Content-type: {$obj["mime"]}");
				echo $image->getImageBlob();
				die();
			} else {
				header("Content-type: image/gif");
				echo file_get_contents("http://www.placehold.it/300x300&text=No+Cover");
			}
		}
	}

}
?>
