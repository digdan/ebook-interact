<?php
	class controllers_MyNodes {
		function newNode($params,$route) {
			$node = model_nodes::create($params["id"],$params["chapter_id"]);
			header("Location: ".Router::instance()->generate("books/node",array('id'=>$params["id"],'node_id'=>$node->id)));
			die();
		}

		function editNodes( $params, $route) {
			if (isset($params["id"])) {
				$book = model_books::instance($params["id"]);
			} else {
				$book = new model_books();
			}

			if (isset($_POST["del_node"])) {
				$node = new model_nodes($_POST['del_node']);
				//TODO CHECK OWNERSHIP
				$reloc = Router::instance()->generate("books/chapters",array("id"=>$book->id));
				$node->delete();
				header("Location: {$reloc}");
				die();
			}
			$nodes = $book->allNodes();

			Scope::instance()->inject("breadcrumb",array(
				"Your Books"=>Router::instance()->generate("books/main"),
				"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
				"Nodes"=>""
			));

			Scope::instance()->inject("book",$book);
			Scope::instance()->inject("nodes",$nodes);
			Scope::instance()->inject("active","chapters");
			echo Scope::instance()->render("books/edit");
		}

		function nodeReorder($params,$route) {
			$book_id = $params['id'];
			$node_ids = explode(",",trim($_GET['order'],","));
			$chapter_id = $start = false;
			$position = 0;
			foreach($node_ids as $node_id) {
				if (is_numeric($node_id)) {
					$node = new model_nodes($node_id);
					if ($chapter_id === FALSE) $chapter_id = $node->chapter_id;
					$position++;
					$node->position = $position;
					if ($start === FALSE) {
						$start = $node->id;
					}
					$node->save();
				}
			}
			$chapter = new model_chapters($chapter_id);
			$chapter->node_start_id = $start;
			$chapter->save();
			echo json_encode(array("success"=>1));
			die();
		}

		function editNode( $params, $route) {
			$book_id = $params["id"];
			if (isset($params["node_id"])) {
				$node_id = $params["node_id"];
			} elseif (isset($params["chapter_id"])) { //Create new node
				$node = new model_nodes();
				$node->book_id = $book_id;
				$node->chapter_id = $params["chapter_id"];
				$node->save();
				header("Location: ".Router::instance()->generate("books/node",array("id"=>$book_id,"node_id"=>$node->id)));
				die();
			}

			$book = model_books::instance($book_id);
			$node = model_nodes::instance($node_id);
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (isset($_POST["del_node"])) {
					$reloc = Router::instance()->generate("books/chapters",array("id"=>$book->id));
					$node->delete();
					header("Location: {$reloc}");
					die();
				}
				if (isset($_POST["name"])) {
					$node->name = $_POST["name"];
					$node->content = $_POST["editor"];
					$node->save();
				}
				if (isset($_POST["pathName"]) and ($_POST["pathName"] != "") and (isset($_POST["pathDestination"]))) {
					$path = new model_paths();
					$path->source_node_id = $node_id;
					$path->text = $_POST["pathName"];
					$dest_id = substr($_POST["pathDestination"],1);
					if (substr($_POST["pathDestination"],0,1) == "n") {
						$path->destination_node_id = $dest_id;
					} elseif (substr($_POST["pathDestination"],0,1) == "c") {
						$path->destination_chapter_id = $dest_id;
					}
					if ($_POST["trigger_node_id"]) {
						$path->trigger_node_id = $_POST["trigger_node_id"];
						$path->trigger_condition = $_POST["trigger_condition"];
					}
					$path->save();
				} elseif (isset($_POST["del_path"])) {
					$d = key($_POST["del_path"]);
					$path = new model_paths($d);
					$g = $path->delete();
				}
			}

			if (isset($_POST['newNode']) and ($_POST['newNode'] == 1)) {
				header("Location: ".Router::instance()->generate("books/node-new",array("id"=>$book->id,"chapter_id"=>$node->chapter_id)));
				die();
			}

			$chapter_id = $node->chapter_id;
			$chapter = model_chapters::instance($chapter_id);

			$paths = model_paths::fromNode($node_id);

			$destination_nodes = $book->allNodes($chapter_id);
			$destination_chapters = model_chapters::chaptersFromBook($book->id);
			$destinations = array_merge($destination_nodes,$destination_chapters);

			Scope::instance()->inject("destinations",$destinations);
			Scope::instance()->inject("destination_nodes",$destination_nodes);
			Scope::instance()->inject("chapter",$chapter);
			Scope::instance()->inject("paths",$paths);
			Scope::instance()->inject("book",$book);
			Scope::instance()->inject("node",$node);
			Scope::instance()->inject("active","chapters");
			Scope::instance()->inject("start",$node->isStart());

			Scope::instance()->inject("breadcrumb",array(
				"Your Books"=>Router::instance()->generate("books/main"),
				"<I class='fa fa-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
				"<I class='fa fa-cubes'></I> <I>".$chapter->name."</I>"=>Router::instance()->generate("books/chapter",array("id"=>$chapter->id)),
				"<I class='fa fa-cube'></I> <I>".$node->name."</I>"=>""
			));

			echo Scope::instance()->render("books/edit");
		}
	}
?>