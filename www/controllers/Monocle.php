<?php
	//Bridge to Monocle JS
	class controllers_Monocle {

		function getComponents($params,$route) {
			$book = new model_books($params['bid']);
			$map = $book->componentMap();
			$out = array();
			foreach($map as $item) {
				if (is_a($item,'model_nodes')) {
					$out[] = "node_".$item->id.".xhtml";
				}
			}
			header("Content-type: application/json, text/javascript");
			echo json_encode($out);
			die();
		}

		function getContents($params,$route) {
			$book = new model_books($params['bid']);
			$map = $book->componentMap();
			$out = array(
				'title'=>'Cover',
				'src'=>'node_cover.xhtml'
			);
			foreach($map as $item) {
				if (is_a($item,'model_nodes')) {
					$title = $item->name;
					$src = "node_".$item->id.".xhtml";
				}
				$out[] = array('title'=>$title,'src'=>$src);
			}
			header("Content-type: application/json, text/javascript");
			echo json_encode($out);
			die();
		}

		function getComponent($params,$route) {
			$book = new model_books($params['bid']);
			$map = $book->componentMap();
			$info = pathinfo($params['cid']);
			$file_name =  basename($params['cid'],'.'.$info['extension']);
			$bparts = explode("_",$file_name);
			$id = $bparts[1];
			switch($bparts[0]) {
				case 'chapter' :
						$chapter = new model_chapters($id);
						$item = new model_nodes( $chapter->node_start_id );
					break;
				case 'node' :
						if ($id == 'cover') {
							//Show book cover

						}
						$item = new model_nodes($id);
					break;
				default :
						$item = false; //Component not found
					break;
			}
			//TODO Access Checks

			$out = array( $params['cid']=>$item->content($map));

			header("Content-type: application/json, text/javascript");
			echo json_encode($out);
			die();
		}

		function getMetaData($params,$route) {
			$out = array(
				'title'=>'A Book',
				'creator'=>'EBI'
			);
			echo json_encode($out);
			die();
		}

	}
?>