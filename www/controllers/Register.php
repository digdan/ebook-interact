<?php
	class controllers_Register {

		function post($params,$route) {
			$config = Config::instance();
			$message = $response = NULL;
			$validate = Validate::instance( 'register', $response, $_POST, ['display_name','email','password','password_confirmation'] );
			if ( $validate->
				check('display_name')->
					fancy('Display Name')->
					is('string')->
					required()->
					min_length(4)->
					max_length(36)->
				check('email')->
					fancy('E-mail')->
					required()->
					regex('email')->
				check('password')->
					fancy('Password')->
					required()->
					min_length(4)->
					max_length(36)->
					same('password_confirmation')->
				run() ) {

				$new_user = new model_users();
				if ( ! $new_user->setEmail($_POST['email'])) {
					$message = "Email address taken";
				} elseif (! $new_user->setPassword($_POST['password'])) {
					$message = "Bad password";
				} elseif ( ! $new_user->setDisplayName($_POST['display_name'])) {
					$message = "Display name taken";
				} else {
					$new_user->save();
					static::verify_email($new_user);
					header("Location: ".Router::instance()->generate('user/register_verify_sent'));die();
				}
			} else {
				$message = $response['message'][0];
			}
			return $message;
		}

		function verify($params,$route) {
			$message = NULL;
			if ($user = model_users::instance($params['user_id'])) {
				if ($user->verification_code == $params['code']) {
					$user->setVerified(true);
					var_dump($user);
					$user->save();
				} else {
					$message = "Invalid verification code";
				}
			} else {
				$message = "Unable to verify account";
			}
			if ( ! is_null($message)) Scope::instance()->inject('message',$message);

			$this->display($params,$route);
		}


		function display($params,$route) {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				Scope::instance()->inject('message', $this->post($params,$route));
			}
			if (Scope::instance()->can_render($route)) {
				echo Scope::instance()->render($route);
			} else {
				return false;
			}
		}

		static function verify_email(model_users $new_user) {
			$config = Config::instance();
			$old = Scope::$current_instance;
			$mail_scope = Scope::instance('verify_email',$config["paths"]["templates"]);
			$mail_scope->inject('code',$new_user->verification_code);
			$mail_scope->inject('url',Router::instance()->generate('user/register_verify',array('user_id'=>$new_user->id,'code'=>$new_user->verification_code)));
			$email = $mail_scope->render($config['register']['verify_email_template']);
			Scope::instance($old);
			return Mail::instance('verify_email',$new_user->email,$config['register']['verify_email_subject'],$email)->setFrom($config['register']['verify_email_sender'],$config['register']['verify_email_sender_address'])->send();
		}
	}
?>