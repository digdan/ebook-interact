<?php
	class controllers_MyChapters {

		function editChapter($params,$route) {
			$chapter = new model_chapters($params['id']);
			$book = new model_books($chapter->book_id);

			Scope::instance()->inject("breadcrumb",array(
				"Your Books"=>Router::instance()->generate("books/main"),
				"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
				"<I class='fa fa-cubes'></I> <I>".$chapter->name."</I>"=>Router::instance()->generate("books/chapter",array("id"=>$chapter->id)),
			));

			if (isset($_POST['del_node'])) {
				$node = new model_nodes($_POST['del_node']);
				if ($node->delete()) { //Node deleted
					//ToDo Message of success
				} else { //Unable to delete node

				}
			}

			$nodes = $chapter->nodesFromChapter();

			Scope::instance()->inject("book",$book);
			Scope::instance()->inject("chapter",$chapter);
			Scope::instance()->inject("active","chapters");
			Scope::instance()->inject("nodes",$nodes);
			echo Scope::instance()->render('books/edit');
		}

		function editChapters($params,$route) {

			if (isset($params["id"])) {
				$book = model_books::instance($params["id"]);
			} else {
				$book = new model_books();
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") { //POST
				if (isset($_POST["cmd"]) and ($_POST["cmd"] == "add_chapter"))  {
					model_chapters::chapterAdd($book->id,$_POST["chapter_name"]);
				}
				if (isset($_POST["chapterName"]) and is_array($_POST["chapterName"])) {
					$chapter_target_id = key($_POST["chapterName"]);
					$chapter_new_name = $_POST["chapterName"][$chapter_target_id];
					foreach($_POST["chapterName"] as $chapter_target_id=>$chapter_new_name) break;
					if (in_array($chapter_target_id,model_chapters::chaptersFromBook( $params["id"], true ))) {
						$chapter = model_chapters::instance($chapter_target_id);
						$chapter->name = $chapter_new_name;
						$chapter->save();
					}
				}
				if (isset($_POST["remove"])) {
					//Validate the chapter belongs to this book.
					if (in_array($_POST["remove"],model_chapters::chaptersFromBook( $params["id"], true ))) {
						$removable = new model_chapters($_POST["remove"]);
						$removable->delete();
					} else {

					}
				}
			}

			Scope::instance()->inject("id",$params["id"]);
			if ($params["id"] > 0) {
				Scope::instance()->inject("book",$book);
			} else {
				Scope::instance()->inject("book",$book);
			}

			Scope::instance()->inject("breadcrumb",array(
				"Your Books"=>Router::instance()->generate("books/main"),
				"<I class='glyphicon glyphicon-book'></I> <I>".$book->title."</I>"=>Router::instance()->generate("books/edit",array("id"=>$book->id)),
				"Node"=>""
			));

			$chapters_ids = model_chapters::chaptersFromBook( $params["id"], true );
			foreach($chapters_ids as $chapter_id) {
				$cur = new model_chapters($chapter_id);
				$cur->nodes = $cur->nodesFromChapter();
				$chapters[] = (array)$cur;
			}
			Scope::instance()->inject("chapters", $chapters );
			Scope::instance()->inject("active","chapters");

			echo Scope::instance()->render('books/edit');
		}

		function chapterOrder($params,$route) {
			$book_id = $params["book_id"];
			$book = model_books::instance($book_id);
			if ($book->canContribute()) {
				foreach(@$_POST as $id=>$order) {
					if (model_chapters::chapterInBook($book_id,$id)) {
						$chapter = model_chapters::instance($id);
						$chapter->setOrder($order);
					}
				}
			}
		}

	}
?>