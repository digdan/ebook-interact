<?php
	class controllers_Settings {

		function main($params,$route=NULL) {
			$scope = Scope::instance();
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$scope->inject("message",$this->settings_post());
			}
			$scope->inject("user",model_users::instanceFromSession());
			echo $scope->render($route);
		}

		function settings_post() {
			$config = Config::instance();
			$message = $response = NULL;
			$validate = Validate::instance( 'register', $response, $_POST, ['display_name','password','password_confirmation','bio','avatar_url','avatar_type'] );
			if ( $validate->
				check('display_name')->
					fancy('Display Name')->
					is('string')->
					required()->
					min_length(4)->
					max_length(36)->
				check('bio')->
					fancy('Bio')->
					max_length(1024 * 64)->
				run() ) {

				$user= new model_users( $_SESSION["user_id"] );

				if (isset($_POST["password"])) {
					$validate = Validate::instance('register',$response,$_POST,['password','password_confirmation']);
					if ($validate->
					check('password')->
						fancy('Password')->
						min_length(4)->
						max_length(36)->
						same('password_confirmation')->
					run() ) {
						if (isset($_POST["password"]) && (! $user->setPassword($_POST['password']))) {
							$message = "Bad password";
						}
					}
				}

				if ($_POST["avatar_type"] == "Uploaded") {
					$size = getimagesize($_POST["avatar_url"]);
					if (($size[0] > $config["avatar"]["max_width"]) || ($size[1] > $config["avatar"]["max_height"])) {
						$message = "Avatar is too large";
					} elseif ( ! $user->setAvatar($_POST["avatar_url"])) {
						$message = "Unable to set avatar";
					}
				} else {
					if ( ! $user->setAvatar('http://www.gravatar.com/avatar/'.md5($user->email))) {
						$message = "Unable to set avatar";
					}
				}

				if (isset($_POST["bio"])) {
					if ( ! $user->setBio($_POST["bio"])) {
						$message = "Unable to update bio";
					}
				}

				if ( ! $user->setDisplayName($_POST['display_name'],true)) {
					$message = "Display name taken";
				} else {
					$user->save();
				}
				return $message;
			} else {
				return $response['message'][0];

			}
		}
	}
?>