<?php

class controllers_Global {

	static function main($route) {
		//Manage Global Scope
		$scope = Scope::instance('default');
		$config = Config::instance('default');
		$user = model_users::instanceFromSession();
		$scope->inject( array(
			"config"=>$config,
			"meta"=>$config["meta"],
			"active"=>"",
			"user"=>$user
		) );
		if ($user->is_admin) {
			$scope->inject( array(
				"admin_log_stats"=>model_adminlog::gather_stats()
			));
		}
	}

}
?>