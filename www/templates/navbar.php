<nav class="navbar navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"> Ebook Interactive </a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li<?= ($route == 'front'?' class="active"':'')?>><a href="<?= Router::instance()->generate('front') ?>"> Home </a></li>
				<?php if (@$_SESSION['user_id']) { ?>
					<li<?= ($route == 'books/main'?' class="active highlight"':' class="highlight"')?>><a href="<?= Router::instance()->generate('books/main') ?>"><span class="glyphicon glyphicon-book"></span> Your Books </a></li>
				<?php } ?>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<?php if (@$_SESSION['user_id']) { ?>
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
								class="glyphicon glyphicon-user"></span> <?=$user->display_name?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<?php
							if ($user->is_admin) { ?>
								<li class="divider"></li>
								<li><a href="<?= Router::instance()->generate("admin")?>"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
							<?php }
							?>
							<li><a href="<?=Router::instance()->generate("user/settings")?>"><span class="glyphicon glyphicon-user"></span> Settings</a></li>
							<li class="divider"></li>
							<li><a href="<?= Router::instance()->generate("user/logout")?>"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
						</ul>
					</li>
				<?php } else { ?>
					<li><a href="<?= Router::instance()->generate("user/login")?>"><span class="glyphicon glyphicon-user"></span> Login</a></li>
					<li><a href="<?= Router::instance()->generate("user/register")?>"><span class="glyphicon glyphicon-pencil"></span> Register</a></li>
				<?php } ?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>
