<nav class="navbar navbar-admin" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/admin"> Admin Dashboard </a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li<?= ($route == 'admin/review'?' class="active"':'')?>><a href="<?= Router::instance()->generate('admin/review') ?>"> Pending Books (<?=$admin_log_stats[model_adminlog::TYPE_BOOK_REVIEW]?>)</a></li>
				<li<?= ($route == 'admin/sales'?' class="active"':'')?>><a href="<?= Router::instance()->generate('admin/sales') ?>"> Sales Report (<?=$admin_log_stats[model_adminlog::TYPE_SALE]?>)</a></li>
				<li<?= ($route == 'admin/support'?' class="active"':'')?>><a href="<?= Router::instance()->generate('admin/support') ?>"> Support Requests (<?=$admin_log_stats[model_adminlog::TYPE_SUPPORT]?>)</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>
