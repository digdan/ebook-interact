<? include 'header.php';

	function trope_field($section,$data) { ?>
		<fieldset>
			<legend><?= $section ?></legend>
			<B><?= $data['name'] ?></B> &mdash; <?= strip_tags($data['desc'],"<strong><div><p>") ?>
		</fieldset>
		<BR><BR>
	<? }
?>
<H1>Trope Generator</H1>
<BR>
<?= trope_field('Setting',$tropes['Setting']) ?>
<?= trope_field('Plot',$tropes['Plot']) ?>
<?= trope_field('Narrative Device',$tropes['Narrative Device']) ?>
<?= trope_field('Hero',$tropes['Hero']) ?>
<?= trope_field('Villain',$tropes['Villain']) ?>
<?= trope_field('Character as Device',$tropes['Character As Device']) ?>
<?= trope_field('Characterization Device', $tropes['Characterization Device']) ?>

<? include 'footer.php' ?>