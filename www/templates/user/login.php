<?php
	include("header.php");
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-lock"></span> Login</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="<?= Router::instance()->generate('user/login')?>">
						<div class="form-group">
							<label for="inputEmail" class="col-sm-3 control-label">
								Email</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="<?= @$_POST["email"]?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword" class="col-sm-3 control-label">
								Password</label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9 text-right">
								<label>Remember me &nbsp;
									<input type="checkbox" value="1" name="remember" <?= ((isset($_COOKIE['remember']) || isset($_POST["remember"])) ? 'checked' : '') ?> />
								</label>
							</div>
						</div>
						<div class="form-group last">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="submit" class="btn btn-success btn-sm"> Sign in</button>
								<button type="reset" class="btn btn-default btn-sm"> Reset</button>
							</div>
						</div>
						<?php
							if (isset($message)) {
								echo "<div class='alert alert-danger alert-dismissable'>{$message}  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>";
							}
						?>
					</form>
				</div>
				<div class="panel-footer text-center">
					Not Registred? <a href="<?= Router::instance()->generate('user/register')?>">Register here</a>
					Problems logging in? <a href="<?= Router::instance()->generate('user/lostpassword')?>">Lost Password?</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$("input.switch").bootstrapSwitch();
		$("#inputEmail").focus();
	});
</script>
<?php
	include("footer.php");
?>
