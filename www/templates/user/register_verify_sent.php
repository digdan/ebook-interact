<?php include("header.php"); ?>
	<H2>Account Verification</H2>
	<P>Your account has been created!</P>
	<P>In order to use your account you must validate your email address. A email has been sent to your registration email address. Please check your email address used during registration to verify your new account.</P>
<?php include("footer.php"); ?>