<?php
include("header.php");
?>
	<form role="form" method="POST" action="<?= Router::instance()->generate('user/settings')?>">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<fieldset><legend>Avatar</legend>
				<div class="form-group">
					<label>
						<input type="radio" name="avatar_type" value="Gravatar" id="radio_gravatar" <?= (strstr($user->avatar_url,'gravatar')?'checked':'')?>> Use <A HREF="http://www.gravatar.com" TARGET="_BLANK">Gravatar</A>

						<div class='text-center col-xs-12'>
							<IMG SRC="http://www.gravatar.com/avatar/<?= strtolower(md5($user->email))?>?s=128" class="img-thumbnail text-center">
						</div>

					</label>
				</div>
				<div class="form-group">
					<label>
						<input type="radio" name="avatar_type" value="Uploaded" <?= (strstr($user->avatar_url,'gravatar')?'':'checked')?>> Use Avatar URL

						<div class='text-center col-xs-12'>
							<img src="<?= (strstr($user->avatar_url,'gravatar')?'http://www.placehold.it/128x128&text=Avatar':$user->avatar_url)?>" class="img-thumbnail text-center">
						</div>
						<div class="form-group">
							<input type="text" name="avatar_url" id="avatar_url" class="form-control input-lg" placeholder="Avatar URL" value="<?= (strstr($user->avatar_url,'gravatar')?'':$user->avatar_url)?>">
						</div>

					</label>
				</div>
				</fieldset>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-1 ">
				<fieldset>
					<legend>Account</legend>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control input-lg" placeholder="Display Name" value="<?=@$user->display_name?>" tabindex="3">
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-6">
						<div class="form-group">
							<input type="password" name="password" id="password" class="form-control input-lg" placeholder="New Password" tabindex="5">
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-6">
						<div class="form-group">
							<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm New Password" tabindex="6">
						</div>
					</div>
				</div>
				</fieldset>
				<fieldset>
					<legend>Bio</legend>
					<div class="row">
						<div class="form-group">
							<textarea name="bio" style="width:100%;" rows="10" placeholder="Write about yourself"><?=$user->bio?></textarea>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div class='row'>
			<div class="col-xs-4 col-xs-offset-4"><input type="submit" value="Save" class="btn btn-primary btn-block btn-lg pull-right" tabindex="7"></div>
			<?php
			if (isset($message)) { ?>
				<BR>
				<div class="row">
					<div class="alert alert-danger"><?=$message?></div>
				</div>
			<? }
			?>
		</div>
	</form>
<?php
include("footer.php");
?>