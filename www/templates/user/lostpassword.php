<?php
	include("header.php");
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-lock"></span> Lost Password</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="<?= Router::instance()->generate('user/lostpassword')?>">
						<div class="form-group">
							<label for="inputEmail" class="col-sm-3 control-label">
								Email</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="<?= @$_POST["email"]?>" required>
							</div>
							<div class='col-sm-9 col-sm-offset-3'><BR>
								<button type="submit" class="btn btn-success btn-sm"> Reset Password </button>
							</div>
						</div>
						<?php
							if (isset($message)) {
								echo "<div class='alert alert-danger alert-dismissable'>{$message}  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>";
							}
						?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>
