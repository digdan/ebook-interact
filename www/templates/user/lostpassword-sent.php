<?php
	include("header.php");
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-lock"></span> Lost Password</div>
				<div class="panel-body">
					<P> An email was sent to your email address to start the password reset process.</P>
					<P>Please check your inbox and/or spam folders. If you are unable to locate the lost password verification email then please try again.</P>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>
