<?php
	include("header.php");
	if (isset($message)) { ?>
	<H2>Verification failed</H2>
	<P>We were unable to verify your account for the following reason : </P>
	<P><?=$message?></P>
<?php } else { ?>
	<H2>Account Verified</H2>
	<P>Your account has been verified. Thank you.</P>
	<P>You may now log in and start using your account</P>
<?php
	}
	include("footer.php");
?>