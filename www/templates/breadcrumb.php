<ol class="breadcrumb">
  <li><a href="/"><i class="fa fa-home"></i></a></li>
  <?php
  $crumb = 0;
  if (isset($breadcrumb)) foreach($breadcrumb as $name=>$link) {
  	$crumb++;
  ?>
  	<li <?= ($crumb == (count($breadcrumb)) ? "class='active'":"")?>><a href="<?=$link?>"><?=$name?></a></li>
  <? } ?>
</ol>