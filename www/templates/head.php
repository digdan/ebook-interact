<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= $meta['title'] ?></title>

	<script type="text/javascript" src="/assets/js/jquery.min.js" ></script>
	<script src="/assets/js/jquery-ui.js"></script>

	<script type="text/javascript" charset="utf8" src="/assets/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.css">

	<script type="text/javascript" src="/assets/js/jquery.form.js" ></script>

	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-theme.min.css">
	<script type="text/javascript" src="/assets/js/bootstrap.min.js" ></script>

	<? /* CDNS
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" ></script>
 	*/ ?>

	<link rel="stylesheet" type="text/css" href="/assets/css/bootswitch.css">
	<script type="text/javascript" src="/assets/js/bootswitch.js"></script>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/custom.css">

	<script src="/assets/tinymce/tinymce.min.js"></script>

</head>
<body class="<?=$routeSlug?>">
