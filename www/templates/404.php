<?php
	ob_end_clean();
	header('HTTP/1.0 404 Not Found');
	include("header.php");
?>
<div class="row">
	<div class="col-md-12">
		<div class="error-template">
			<h1>
				Oops!</h1>
			<h2>
				404 Not Found</h2>
			<div class="error-details">
				Sorry, an error has occured, Requested page not found!
			</div>
			<BR>
			<div class="error-actions">
				<a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
					Take Me Home </a>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
	exit;
?>
