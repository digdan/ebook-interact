<?php
	define('APP_ROOT',__DIR__);

	//Define library path
	set_include_path( get_include_path() . PATH_SEPARATOR . __DIR__ . DIRECTORY_SEPARATOR . "libs" );

	//Initiate Autoloader
	include("autoload.php");

	//Load Logger
	Analog\Analog::handler( Analog\Handler\Multi::init (array(
		Analog\Analog::URGENT	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/error.log'),
		Analog\Analog::ALERT	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/error.log'),
		Analog\Analog::CRITICAL	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/error.log'),
		Analog\Analog::ERROR	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/error.log'),
		Analog\Analog::WARNING	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/debug.log'),
		Analog\Analog::NOTICE	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/debug.log'),
		Analog\Analog::INFO	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/debug.log'),
		Analog\Analog::DEBUG	=> Analog\Handler\File::init(dirname(APP_ROOT).'/logs/debug.log'),
	)));

	Analog\Analog::register(); //Register as our error handler

	//Initiate Instances through Singleton Factories
	$config = Config::instance('default','./config.php');
	Scope::setBase($config["paths"]["templates"]);
	$scope = Scope::instance('default');
	$mdb = new Mongo ("mongodb://localhost:27017",array('connect'=>true) );
	DBObject::setDB( $mdb->ebi ); //Set Mongo ORM DB Handler

	DB::instance('main', $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );

	$router = Router::instance('html',include 'routes.php');
	$cache = Cache::instance('default',$config["cache"]["host"],$config["cache"]["port"]);
	$fkeeper = FKeeper::instance('default',$config["books_directory"]); //File Keeper

	session_start();

	try {
		$route = $router->match();
		controllers_Global::main($route);
		if (isset($route)) {
			$scope->inject( array("route"=>$route["name"],"routeSlug"=>Utils::slugify($route["name"])) ); //Inject Route into global vars
			if ($router->execute( $route ) === FALSE) {
				echo Scope::instance()->render('404');
			}
		} else {
			echo Scope::instance()->render('404');
		}
	} catch ( Exception $e ) {
		echo Scope::instance()->render( 'error', array( "error" => $e->getMessage() ) );
	}

?>
