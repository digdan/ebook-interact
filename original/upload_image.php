<?
define("LOGIN_REQUIRED",TRUE);
include("header_blank.php");
$template = "blank.tpl";
$content["CONTENT"] = "{IMAGE_UPLOAD}";

$iu_form["SCRIPT"] = "";
$iu_form["IMG_PREVIEW"] = "<IMG STYLE=\"border:1px solid black\" border=\"0\" SRC=\"images/missing.jpg\">";
$iu_form["IMG_PREVIEW"] .= "<CENTER><SMALL>preview</SMALL></CENTER>";
$iu_form["NOTE"] = "";

if ($_FILES["photofile"]) {
	$photo = new photofile($_FILES["photofile"]);
	$db->save_object($photo);
	$iu_form["IMG_PREVIEW"] = "<IMG STYPE=\"border:1px solid black\" border=\"0\" SRC=\"image_preview.php?pid={$photo->data["id"]}&s=1\">";
	$iu_form["IMG_PREVIEW"] .= "<CENTER><SMALL><A HREF=\"image_preview.php?pid={$photo->data["id"]}\" TARGET=\"_blank\">preview</A></SMALL></CENTER>";
	$iu_form["NOTE"] = "Save Instance to use this picture";

	//Tell script to set the new photo id to its parent

	$iu_form["SCRIPT"] = "<SCRIPT LANGUAGE=\"JAVASCRIPT\">parent.document.storyform.PHOTO_ID.value='{$photo->data["id"]}';</SCRIPT>\n";
}

if ($_GET["pid"]) {
	$iu_form["IMG_PREVIEW"] = "<IMG STYPE=\"border:1px solid black\" border=\"0\" SRC=\"image_preview.php?pid={$_GET["pid"]}&s=1\">";
	$iu_form["IMG_PREVIEW"] .= "<CENTER><SMALL><A HREF=\"image_preview.php?pid={$_GET["pid"]}\" TARGET=\"_blank\">preview</A></SMALL></CENTER>";
}


$tpl->set_file("IMAGE_UPLOAD","upload_image.tpl");


if (is_array($iu_form)) {
	foreach($iu_form as $k=>$v) {
		$tpl->set_var($k,$v);
	}
}

$tpl->process("IMAGE_UPLOAD","IMAGE_UPLOAD",false,false,false,false);
include("footer.php");
?>
