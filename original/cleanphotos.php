#!/usr/local/bin/php -q
<?
	if (!$argv[0]) { //Command line only please
		die();
	}
	/* Cron Script for removing unused photos
		
	*/

	define("LOGIN_REQUIRED",false);
	include("header_blank.php");


	//Load Images from Instances
	$db->query("SELECT photofiles.uname from photofiles,instances where instances.photofile_id = photofiles.id");
	if (is_array($db->results)) {
		foreach($db->results as $k=>$v) {
			$saves[] = $v["uname"];
		}
	}

	$delcount = "0";

	if (is_array($saves)) {
		$files = glob("{$config["user_images"]}*");
		foreach($files as $k=>$v) {
			if (($v != ".") && ($v != "..")) {
				$pparts = split("/",$v);
				$fname = $pparts[count($pparts)-1];
				$fprint = substr($fname,0,32);
				if (!in_array($fprint,$saves)) {
					$killphoto = new photofile();
					if ($killphoto->loadByUname($fprint)) { //Exists in DB
						$killphoto->remove();
						$delcount++;
						unset($killphoto);
					} else { // Doesn't Exist in DB
						if (file_exists($v)) {
							unlink($v);
						}
					}
				}
			}
		}
	}

	echo "{$delcount} images delete.\n\r";

?>
