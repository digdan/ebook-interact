1.) By entering our site, I represent that I have read and agree to all of the terms 
    of this Membership Agreement I certify that I am over 18 years old (over 21 in AL, 
    MS, NE and WY). 

2.) I am familiar with and I understand the standards and laws of the community in which 
    I live regarding sexually-oriented media. I represent that, based on my familiarity 
    with these standards and laws of my community, by requesting and receiving any of 
    the adult material I will not be violating any of these standards 
    and/or laws. I understand that by accepting the terms of this agreement I will hold 
    {DOMAIN}, its directors, officers, employees, agents, successors, licensees, 
    and assigns harmless from and against any and all claims, liabilities, damages, costs 
    or expense related to my requesting, receiving or possessing materials contained or 
    offered on its site.

3.) I understand and I agree to the terms of membership, namely:
    a) that if I join as a "CHARTER MEMBER" I will receive all services of this
       website free of charge for the duration of the existence of this site, unless
       such service is revoked for violation of these terms of service.
    b) that CHARTER MEMBER status, once granted, will not be revoked.
    c) that in the future it is likely that new members (non-charter members) will
       be charged a monthly fee for writing and/or reading journals on this site, including
       for reading any that I may write.

4.) I agree that {DOMAIN} has the right to change all pricing, content, and hours of 
    service at any time, and to immediately terminate service to any person without cause 
    or prior notice.

5.) I agree not to use {DOMAIN} to send, or submit for public posting any abusive, 
    obscene, threatening, libelous, slanderous, or illegal matter or material, containing 
    blatant expressions of bigotry, racism or hate. I further agree not to send, or submit 
    for public posting any copyrighted matter or material, without the specific authority 
    of the copyright owner. I agree to indemnify and hold {DOMAIN} free and harmless
    from any and all claims, suits, damages, costs and expenses (including attorneys' fees) arising
    out of or in any manner related to any matter or material sent by me or submitted by me
    for public posting. It is acknowledged that {DOMAIN} has the right to cancel or
    discontinue, in its sole discretion, and without cause, the posting of any matter or
    material sent by me or submitted by me.

    I acknowledge that {DOMAIN} does not pre-screen Content, but that {DOMAIN} 
    and its designates shall have the right (but not the obligation) on their sole discretion
    to refuse or remove any Content that is available through the Service. Without limiting
    the foregoing, {DOMAIN} and its designates shall have the right to remove any
    content that violates the TOS or is otherwise objectionable. I agree that I must evaluate,
    and bear all risks associated with, the use of any content, including any reliance on
    the accuracy, completeness, or usefulness of such content. Furthermore, {DOMAIN} 
    reserves the right to limit access to my journal, if found in violation of the TOS, by
    removing the journal and related user information from the member directory, search engine,
    and all other methods used in conjunction with finding journals and users. 

6.) I understand and agree that no warranty is made by {DOMAIN} regarding the 
    {DOMAIN} service, and {DOMAIN} hereby expressly disclaims: (1) any and 
    all warranties as to the availability, accuracy, or content of information, products, 
    or services available through the {DOMAIN} service; and (2) any warranties of 
    merchantability or fitness for a particular purpose. I further understand and agree 
    that any liability of {DOMAIN}, its officers, directors, employees, agents, 
    or independent contractors, including without limitation any liability for damages 
    caused or allegedly caused by any failure of performance, error, omission, interruption, 
    deletion, defect, delay in operation or transmission, computer virus, communications 
    line failure, theft, destruction, alteration, or use of (or unauthorized access to) 
    records, whether for breach of contract, breach of warranty, negligence or other 
    tortuous behavior, shall be strictly limited to the amount paid to {DOMAIN} 
    by or on behalf of the claimant for {DOMAIN} service membership fees for 
    the month in which the claimed injury or damage occurred. Some states DO NOT allow 
    the limitation or exclusion of liability for incidental or consequential damages for 
    the above limitation or exclusion. This may not apply to me.

7.) Subscribers are responsible for providing all personal computer and communications 
    equipment necessary to gain access to the Service. Access to and use of the Service is 
    through a combination of an ID, and a password. Each Subscriber must keep his password 
    strictly confidential. Unauthorized access to the Service is a breach of this Agreement 
    and a violation of law.

8.) Subscriber may not assign or transfer membership to any other person or entity. 
    {DOMAIN} however, reserves the right to assign or transfer membership to a party 
    of its choice. Under such event, the terms, conditions and content will remain 
    substantially similar.

9.) {DOMAIN} claims no ownership or control over any Content posted by its users.
    The author retains all patent, trademark, and copyright to all Content posted within
    available fields, and is responsible for protecting those rights, but is not entitled
    to the help of the {DOMAIN} staff in protecting such Content. 

10.) The material on the Service is for the private, non-commercial enjoyment of 
     Subscribers only. Any other use is prohibited.

11.) This Agreement contains the entire agreement between the Subscriber and {DOMAIN} 
     regarding the use of the Service, and supersedes all prior written and oral 
     understandings and writings, and may only be amended upon notice by {DOMAIN} 
     to Subscribers. Unless otherwise explicitly stated, provisions of this Agreement 
     shall survive its terminations. The Agreement shall be governed by the laws of the 
     State of California.

12.) I am hereby advised that any purchases that I make of products or services offered 
     by use of the site are final.
