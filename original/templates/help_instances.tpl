<DIV CLASS="help_header">CREATING INSTANCE</DIV>
When creating a new story you should already have the idea or basic theme in your head. Remember the first thing someone will see is the name of your story, and then the summary.
<BR><BR>
Try staying in the same font throughout the whole story and its summary. Also creating color schemes for certain areas can add a nice effect.
