<SCRIPT SRC="js/validation.js"></SCRIPT>
<SCRIPT>
	function checkForm(theForm) {
		var why = "";
		updateRTE('Bio');
		why += checkUsername(theForm.Username.value);
		why += checkPassword(theForm.Password.value);
		why += checkEmail(theForm.Email.value);
		why += isDifferent(theForm.Password.value,theForm.checkPassword.value,'Passwords');
		if (!theForm.agreement.checked) {
			why += "You need to agree to the terms of service.\n";
		}
		if (why != "") {
			alert(why);
			return false;
		}
		return true;
	}

	function forceSelect(sAvatar) {
		frames['AvatarFrame'].setsel(sAvatar);
	}
        initRTE("images/editor/", "", "", true);
</SCRIPT>
<FORM METHOD="POST" ACTION="?" NAME="register_form" onSubmit="return checkForm(register_form);">
<INPUT TYPE="HIDDEN" name="Avatar" value="">

<TABLE ALIGN="CENTER" BORDER="0"><TR><TD>

<TABLE class="formbox" ALIGN="CENTER" BORDER="0">
	<TR><TD class="formlabel">Username : </TD><TD><INPUT TYPE="TEXT" NAME="Username" value="{USERNAME_VALUE}"}></TD></TR>
	<TR><TD class="formlabel">Password : </TD><TD><INPUT TYPE="PASSWORD" NAME="Password" value="{PASSWORD_VALUE}"></TD></TR>
	<TR><TD class="formlabel">Password Again : </TD><TD><INPUT TYPE="PASSWORD" NAME="checkPassword" value="{PASSWORD_VALUE}"></TD></TR>
	<TR><TD class="formlabel">Email Address : </TD><TD><INPUT TYPE="TEXT" name="Email" value="{EMAIL_VALUE}"></SMALL></TD></TR>
        <TR><TD class="formlabel" COLSPAN="2">
        <TABLE><TR><TD>Selected Avatar:<BR><IMG NAME="avatar_preview" SRC=""></TD><TD>
        <IFRAME STYLE="border:1px solid black" SCROLLING="AUTO" ID="AvatarFrame" NAME="AvatarFrame" FRAMEBORDER="0" HEIGHT="150" ALIGN="CENTER" SRC="avatars.php"></IFRAME>

        </TD></TR>
        <TR><TD COLSPAN="2" ALIGN="CENTER"><BR>
	<DIV class="editbox">
        Bio :<BR>
	<TEXTAREA name="Bio" Cols="42" Rows="13">{BIO_VALUE}</TEXTAREA><BR><BR>
<BR></DIV></TD></TR></TABLE>
        </TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER"><DIV ID="loading_anim"><IMG SRC="images/loading.gif"><BR><B>Loading Avatars, please wait.<BR><SMALL>This may take awhile</SMALL></B></DIV></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER" CLASS="error">{WHY}</TD></TR>

</TABLE>

</TD><TD VALIGN="TOP" HEIGHT="100%">

<TABLE class="helpbox" WIDTH="265">
<TR><TD ALIGN="CENTER">
{HELP_REGISTER_USER}
</TD></TR>
<TR><TD>
<HR>
<CENTER><B>Terms of Service Agreement</B></CENTER>
<TEXTAREA NOSWAP name="agree" rows="25" STYLE="width:260px" READONLY WRAP=OFF>
{AGREEMENT}
</TEXTAREA>
</TD></TR>
<TR><TD><INPUT TYPE="CHECKBOX" name="agreement" value="1" {AGREEMENT_VALUE}> I have read, accepted, and ackowledge to the Terms of Agreement
</TD></TR></TABLE>

</TD></TR></TABLE>
</FORM>
