<SCRIPT SRC="js/validation.js"></SCRIPT>
<SCRIPT>
	function checkForm(theForm) {
		var why = "";
		why += checkPassword(theForm.oldPassword.value);
		why += checkPassword(theForm.Password.value);
		why += isDifferent(theForm.Password.value,theForm.checkPassword.value,'Passwords');
		if (why != "") {
			alert(why);
			return false;
		}
		return true;
	}

	function forceSelect(sAvatar) {
		frames['AvatarFrame'].setsel(sAvatar);
	}
</SCRIPT>
<FORM METHOD="POST" ACTION="?" NAME="register_form" onSubmit="if (checkForm(register_form)) { register_form.submit()} else {return false;}">
<INPUT TYPE="HIDDEN" name="Avatar" value="">

<TABLE ALIGN="CENTER" BORDER="0"><TR><TD>

<TABLE class="formbox" ALIGN="CENTER">
	<TR><TD class="formlabel">Username : </TD><TD><INPUT TYPE="TEXT" NAME="Username" value="{USERNAME_VALUE}"} DISABLED></TD></TR>
	<TR><TD class="formlabel">Old Password : </TD><TD><INPUT TYPE="PASSWORD" NAME="oldPassword"></TD></TR>
	<TR><TD class="formlabel">Password : </TD><TD><INPUT TYPE="PASSWORD" NAME="Password" value="{PASSWORD_VALUE}"></TD></TR>
	<TR><TD class="formlabel">Password Again : </TD><TD><INPUT TYPE="PASSWORD" NAME="checkPassword" value="{PASSWORD_VALUE}"></TD></TR>
	<TR><TD class="formlabel">Email Address : </TD><TD><INPUT TYPE="TEXT" name="Email" value="{EMAIL_VALUE}" DISABLED></SMALL></TD></TR>
	<TR><TD class="formlabel" COLSPAN="2">
	<TABLE><TR><TD>Selected Avatar:<BR><IMG NAME="avatar_preview" SRC=""></TD><TD>
	<IFRAME STYLE="border:1px solid black" SCROLLING="AUTO" ID="AvatarFrame" NAME="AvatarFrame" FRAMEBORDER="0" HEIGHT="150" ALIGN="CENTER" SRC="avatars.php?f=1"></IFRAME>
	
	</TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER">
<BR><DIV class="editbox">
	Bio :<BR>
	<TEXTAREA name="Bio" Cols="42" Rows="13">{BIO_VALUE}</TEXTAREA><BR><BR>
	</TD></TR></TABLE></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER"><DIV ID="loading_anim"><IMG SRC="images/loading.gif"><BR><B>Loading Avatars, please wait.<BR><SMALL>This may take awhile</SMALL></B></DIV></TD></TR>
</TABLE>
</TD><TD WIDTH="220" VALIGN="TOP" class="helpbox">
<DIV CLASS="error">{WHY}</DIV>
<DIV CLASS="success">{SUCCESS}</DIV>
<DIV CLASS="help">{HELP_MYACCOUNT}</DIV>
</TD></TR></TABLE>
</FORM>
