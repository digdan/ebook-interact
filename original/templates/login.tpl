<SCRIPT SRC="js/validation.js"></SCRIPT>
<SCRIPT>
	function checkForm(theForm) {
		var why = "";
		why += checkUsername(theForm.Username.value);
		why += checkPassword(theForm.Password.value);
		if (why != "") {
			alert(why);
			return false;
		}
		return true;
	}
</SCRIPT>
<FORM METHOD="POST" ACTION="login.php" NAME="login_form" onSubmit="if (checkForm(login_form)) { login_form.submit()} else {return false;}">
<TABLE class="formbox" ALIGN="CENTER">
	<TR><TD class="error" align="center" colspan="2">{LOGIN_ERROR}</TD></TR>
	<TR><TD class="formlabel">Username : </TD><TD><INPUT TYPE="TEXT" NAME="Username"></TD></TR>
	<TR><TD class="formlabel">Password : </TD><TD><INPUT TYPE="PASSWORD" NAME="Password"></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="SUBMIT" name="command" value="Login"></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER" CLASS="formlabel">Not a signed-up yet? <A HREF="register.php">Register</A> for free<BR><A HREF="lost_password.php">Lost Password</A></TD></TR>
</TABLE>
</FORM>
