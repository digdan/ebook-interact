Thank you!</B> for taking interest in {APP_TITLE}!

Your Login Information :
* Username : {USERNAME}
* Password : {PASSWORD}

To activate your account you will need to copy and paste the following link into your browser :

http://{LINK}verify.php?id={ID}&verify={VERIFY}

Thank You,
The {APP_TITLE} Team
