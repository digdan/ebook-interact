<P><B>Thank you!</B> for taking interest in {APP_TITLE}!</P>
<BR>
Your Login Information
<UL>
<LI>Username : {USERNAME}</LI>
<LI>Password : {PASSWORD}</LI>
</UL>
<BR>
To activate your account you will need to click the following link :<BR>
<A HREF="{LINK}verify.php?id={ID}&verify={VERIFY}">Verify Account</A><BR><BR>
If that does not work then copy and paste the following link into your browser :<BR>
{LINK}verify.php?id={ID}&verify={VERIFY}<BR>

<BR><BR>
<I>Thank you,</I><BR>
The {APP_TITLE} Team
