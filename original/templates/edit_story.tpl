<SCRIPT LANGUAGE="JavaScript">
        initRTE("images/editor/", "", "", true);
	function formSubmit() {
		updateRTE('Summary');
		if (storyform.name.value == "") {alert('Please enter a story name.');return false;}
	}
</SCRIPT>
<CENTER>
<H2>Edit Story</H2>
<FORM METHOD="POST" ACTION="?" NAME="storyform" onSubmit="return formSubmit();" enctype="multipart/form-data">
<INPUT TYPE="HIDDEN" NAME="STORY_ID" VALUE="{STORY_ID}">
<TABLE><TR><TD VALIGN="TOP">
<TABLE>
<TR><TD>Story Name :</TD><TD><INPUT TYPE="TEXT" size="50" name="name" value="{STORY_NAME}"></TD></TR>
<TR><TD COLSPAN="2">
<DIV class="editbox">
<B>Story Summary :</B><BR>
<CENTER>
<TEXTAREA name="Summary" Cols="42" Rows="13">{SUMMARY_VALUE}</TEXTAREA><BR><BR>
<BUTTON onClick="newwindow = window.open('preview-bbcode.php','preview-window','left=20,top=20,width=500,height=500,scrollbars=yes,toolbar=0,resizable=0');document.miniform.Message.value = storyform.Summary.value;if (newwindow.location) { document.miniform.submit(); } else { alert('Preview window was blocked. Please set your pop-up blockers to not block popups from this site.'); } return false;">Preview</BUTTON>
</CENTER>
<BR></DIV>
</TD></TR>
<TR><TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="SUBMIT" name="command" value="Save Story" onClick="storyform.target='';storyform.action='?'"></TD></TR>
</TABLE>
</TD><TD VALIGN="TOP" WIDTH="220" CLASS="helpbox">
<DIV CLASS="error">{error}</DIV>
<DIV CLASS="help">{HELP_EDIT_STORY}</DIV>
</TD></TR></TABLE>
</FORM>
<FORM name="miniform" method="POST" TARGET="preview-window" ACTION="preview-bbcode.php">
	<INPUT TYPE="hidden" name="Message" value="">
</FORM>
</CENTER>
