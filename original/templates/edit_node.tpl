<TABLE class="editor_storymenu"><TR><TD>{STORY_INFO}</TD></TR></TABLE>
<CENTER>
<H2>Edit Node </H2>
<FORM METHOD="POST" ACTION="?" NAME="storyform" onSubmit="return formSubmit();" enctype="multipart/form-data">
<INPUT TYPE="HIDDEN" NAME="NODE_ID" VALUE="{NODE_ID}">
<TABLE><TR><TD VALIGN="TOP">
<TABLE>
<TR><TD>Node Name :</TD><TD><INPUT TYPE="TEXT" size="50" name="name" value="{NODE_NAME}"></TD></TR>
<TR><TD>Node  Type : </TD><TD><SELECT name="Node_Type">
<OPTION VALUE="1">Simple Node</OPTION>
</SELECT></TD></TR>
<TR><TD>
Destination Instance
</TD><TD>
<SELECT name="Destination_Instance">
<OPTION value="0">-- New Instance --</OPTION>
{OTHER_INSTANCES}
</SELECT>
</TD></TR>
<TR><TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="SUBMIT" name="command" value="Save Node" onClick="storyform.target='';storyform.action='?'"></TD></TR>
</TABLE>
</TD><TD VALIGN="TOP" WIDTH="220" CLASS="helpbox">
<DIV CLASS="error">{error}</DIV>
<DIV CLASS="help">{HELP_EDIT_NODE}</DIV>
</TD></TR></TABLE>
</FORM>
</CENTER>
