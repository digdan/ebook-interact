<SCRIPT LANGUAGE="JavaScript">
        initRTE("images/editor/", "", "", true);
	function formSubmit() {
		updateRTE('Description');
		if (storyform.name.value == "") {alert('Please enter an instance name.');return false;}
	}
</SCRIPT>
<CENTER>
<TABLE class="editor_storymenu"><TR><TD>{STORY_INFO}</TD></TR></TABLE>
<H2>Edit Instance </H2>
<FORM METHOD="POST" ACTION="?" ENCTYPE="multipart/form-data" NAME="storyform" onSubmit="return formSubmit();" enctype="multipart/form-data">
<INPUT TYPE="HIDDEN" NAME="INSTANCE_ID" VALUE="{INSTANCE_ID}">
<INPUT TYPE="HIDDEN" NAME="NODE_ID" VALUE="{NODE_ID}">
<TABLE><TR><TD VALIGN="TOP">
<TABLE>
<TR><TD>Instance Name :</TD><TD><INPUT TYPE="TEXT" size="50" name="name" value="{INSTANCE_NAME}"></TD></TR>
<TR><TD>Nick Name :</TD><TD><INPUT TYPE="TEXT" size="50" name="nickname" value="{NICK_NAME}"></TD></TR>
<TR><TD COLSPAN="2">
<DIV class="editbox">
        <CENTER><B>Instance Description :</B></CENTER><BR>
<TEXTAREA name="Description" Cols="42" Rows="13">{DESCRIPTION_VALUE}</TEXTAREA><BR><BR>
<BR></DIV>
</TD></TR>
<TR class="editbox"><TD colspan="2">
<INPUT TYPE="HIDDEN" NAME="PHOTO_ID" VALUE="{PHOTO_ID}">
<IFRAME CLASS="upload_image_window" SCROLLING="OFF" FRAMEBORDER="0" SRC="upload_image.php?pid={PHOTO_ID}">
</IFRAME>
</TD></TR>
<TR><TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="SUBMIT" name="command" value="Save Instance" onClick="storyform.target='';storyform.action='?'"></TD></TR>
</TABLE>
</TD><TD VALIGN="TOP" WIDTH="220" CLASS="helpbox">
<DIV CLASS="error">{error}</DIV>
<DIV CLASS="help">{HELP_EDIT_INSTANCE}</DIV>
</TD></TR></TABLE>
</FORM>
</CENTER>
