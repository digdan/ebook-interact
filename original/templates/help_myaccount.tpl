<DIV CLASS="help_header">Modifying your Account</DIV>
At the account modification screen you can change your password, your avatar and your bio.
<BR><BR>
<I>Password</I> must be :<BR>
&middot;6 to 16 characters in length.<BR>
&middot;Can only contain numbers and/or letters.<BR>


