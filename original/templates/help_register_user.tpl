<DIV CLASS="help_header">REGISTERING</DIV> 
<B><I>Valid email address required to register.</I></B><BR>
<DIV ALIGN="LEFT">
<P><I>Username</I> must be :<BR>
&middot; Between 4 and 10 characters in length<BR>
&middot; Contain numbers and/or letters</P>

<P><I>Password</I> must be :<BR>
&middot; Between 6 and 16 characters in length<BR>
&middot; Contain numbers and/or letters.
</P>

<P>If you do not see an Avatar that you like don't worry. You will be able to create your own 100x100px avatars in the near future.</P>
<P>You must also read and agree to the terms of service agreement below.</P>
 </DIV>


