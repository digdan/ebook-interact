<?
define("LOGIN_REQUIRED",true);
include("header.php");

$edit["error"] = "";

//Try to gather Story ID from the session, if it is not there search $_GET
if ($storyid = $sess->get("story_id")) {
} else {
	if ($_GET["sid"]) {
		// Add ownership checking here
		$sess->register("story_id",$_GET["sid"]);
		$storyid = $_GET["sid"];
	} else { // Not in GET
		if ($_POST["sid"]) {
			$storyid = $_POST["sid"];
		} else { // Not in Post
			if ($_GET["iid"]) {
				$db->query("SELECT story_id from instances where id = {$_GET["iid"]}");
				$storyid = $db->results[0]["story_id"];
			} else { //Unreferancable
				user_error("Instance has no referancing to Story");
			}
		}
	}
}


$edit["INSTANCE_NAME"] = "";
$edit["DESCRIPTION_VALUE"] = "";
$edit["NICK_NAME"] = "";
$edit["INSTANCE_CONTROL"] = "";
$edit["PHOTO_ID"] = "";
$edit["NODE_ID"] = "";


if ($_GET["iid"]) {
	$preload_id = $_GET["iid"];
}
if ($_POST["INSTANCE_ID"]) {
	$preload_id = $_POST["INSTANCE_ID"];
}
if ($_POST["instance_id"]) {
	$preload_id = $_POST["instance_id"];
}
if ($preload_id) {
	$edit["INSTANCE_ID"] = $preload_id;
	$sess->register("instance_id",$preload_id);
} else {
	$edit["INSTANCE_ID"] = "";
}

if ($_GET["nid"]) {
	$edit["NODE_ID"] = $_GET["nid"];
}

/* Now Load the Story */
$story = new story($storyid);

if ($_POST["command"] == "Save Instance") {
		global $db;
		$newname = $_POST["name"];
		$nickname = $_POST["nickname"];
		$description = $_POST["Description"];
		$ownerid = $userinfo["id"];
		if ($_POST["PHOTO_ID"]) {
			$photo_id = $_POST["PHOTO_ID"];
		} else {
			$photo_id = "0";
		}


		if (!$ownerid) {
			user_error("Unable to allocate parent story.");
		}

		$now = time();
		if ($_POST["INSTANCE_ID"]) {
			$instance = new instance($_POST["INSTANCE_ID"]);
			$values = array (
				"name"=>$newname,
				"nickname"=>$nickname,
				"story_id"=>$storyid,
				"owner_id"=>$ownerid,
				"description"=>$description,
				"last_updated"=>$now,
				"id"=>$_POST["INSTANCE_ID"],
				"photofile_id"=>$photo_id
			);
		} else {
			$values = array (
				"name"=>$newname,
				"nickname"=>$nickname,
				"story_id"=>$storyid,
				"owner_id"=>$ownerid,
				"flags"=>"0",
				"description"=>$description,
				"create_date"=>$now,
				"photofile_id"=>$photo_id,
				"last_visit"=>"0"
			);
		}
		$instance = new instance();
		$instance->set_value($values);

		if (!$db->save_object($instance)) {
			$edit["error"] = "Unable to save instance.";
			$edit["INSTANCE_NAME"] = $_POST["name"];
			$edit["NICK_NAME"] = $_POST["nickname"];
			$preDescription = trim($_POST["Description"]);
			if (!$preDescription) {
				$edit["DESCRIPTION_VALUE"] = "";
			} else {
				$edit["DESCRIPTION_VALUE"] = $preDescription;
			}
		} else { //Successful Save


			if ($_POST["NODE_ID"]) {
				$node = new node();
				$db->load_object($_POST["NODE_ID"],$node);
				$node->data["destination_id"] = $instance->data["id"];
				$db->save_object($node);
			}
		}

		if (!$_POST["INSTANCE_ID"]) {
			$edit["INSTANCE_ID"] = $instance->data["id"];
		}

		if ($story->data["starting_instance_id"] == "0") {
			$story->data["starting_instance_id"] = $instance->data["id"];
			$db->save_object($story);
		}

		$edit["INSTANCE_NAME"] = $instance->data["name"];
		$edit["NICK_NAME"] = $instance->data["nickname"];
		$edit["DESCRIPTION_VALUE"] = makeFormSafe($instance->data["description"]);
}

if ($preload_id) {
	$instance = new instance();
	$db->load_object($preload_id,$instance);
	$edit["DESCRIPTION_VALUE"] = makeFormSafe($instance->data["description"]);
	$edit["INSTANCE_NAME"] = $instance->data["name"];
	$edit["NICK_NAME"] = $instance->data["nickname"];
	$edit["INSTANCE_ID"] = $instance->data["id"];
}

if ($instance->data["id"]) { //Instances Exists
	if ($instance->data["photofile_id"]) {
		$edit["PHOTO_ID"] = $instance->data["photofile_id"];
	} else {
		$edit["INSTANCE_PHOTO"] = "<IMG SRC=\"images/missing.jpg\" STYLE=\"border:1px solid black\">";
	}
	$nodeBuffer = "<CENTER><A HREF=\"edit_node.php?iid={$instance->data["id"]}\">Create Outbound Node</A></CENTER>";
	$nodeBuffer .= "<IFRAME CLASS=\"node_window\" FRAMEBORDER=\"0\" SCROLLING=\"on\" SRC=\"display_nodes.php?iid={$instance->data["id"]}\"></IFRAME>";
	$edit["NODE_CONTROL"] = $nodeBuffer;
	if ($story->data["starting_instance_id"] == $instance->data["id"]) {
		$instanceBuffer = "";
	} else {
		$instanceBuffer = "<CENTER><Button onClick=\"if (confirm('Delete instance and all connecting nodes?')) { document.location.href='remove_instance.php?iid={$instance->data["id"]}';return false;} else { return false;};\">Delete Instance</Button></CENTER>";
	}
	$edit["INSTANCE_CONTROL"] = $instanceBuffer;
} else {
	$edit["NODE_CONTROL"] = "<CENTER><I>Node Creation not allowed at this time</I></CENTER>";
}


$storyinfo = "<TABLE class=\"editor_storymenu\"><TR><TD ALIGN=\"left\"><B>Story :</B> {$story->data["story_name"]} <BUTTON onClick=\"document.location.href='edit_story.php?sid={$story->data["id"]}';\">Edit</BUTTON><SMALL></TD>";
if ($story->data["starting_instance_id"] == "0") {
	$storyinfo .= "<TD ALIGN=\"right\">Creating the starting instance</TD></TR></TABLE>";
} else {
	// Build list of instances.
	$instance_list = $story->renderInstances("instance_id",$instance->data["id"],"onChange=\"document.location.href='edit_instance.php?iid='+this.value;\"");
	if ($story->data["starting_instance_id"] == $instance->data["id"]) {
		$storyinfo .= "<TD ALIGN=\"right\">Starting Instance | Current Instance : {$instance_list}</TD></TR></TABLE>";
	} else {
		$storyinfo .= "<TD ALIGN=\"right\">Current Instance : {$instance_list}</TR></TABLE>";
	}
}


$tpl->set_var("STORY_INFO",$storyinfo);

$template = "main.tpl";
$content["CONTENT"] = "{EDIT_STUB}";

$tpl->set_file("EDIT_STUB","edit_instance.tpl");
foreach($edit as $k=>$v) {
	$tpl->set_var($k,$v);
}
$tpl->set_file("HELP_EDIT_INSTANCE","help_edit_instance.tpl");
$tpl->process("HELP_EDIT_INSTANCE","HELP_EDIT_INSTANCE",false,false,false,false);
$tpl->process("EDIT_STUB","EDIT_STUB",false,false,false,false);
include("footer.php");
?>
