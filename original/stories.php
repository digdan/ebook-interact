<?
define("LOGIN_REQUIRED",false);
include("header.php");
$template = "main.tpl";

if (!$start) {
	$start = "0";
}

$db->query("SELECT stories.story_name,stories.story_summary,stories.id as storyid,users.username,users.id as userid from users,stories where stories.owner_id = users.id order by stories.id desc limit {$start},25");

$count = "0";
if (is_array($db->results)) {
	$buffer = "<TABLE WIDTH=\"400\">";
	foreach($db->results as $k=>$v) {
		$count++;
		$num = $count+$start;
		$preview = shorten($v["story_summary"],300);
		$buffer .= "<TR valign=\"top\"><TD>{$num}.</TD><TD><A TARGET=\"_BLANK\" HREF=\"reader.php?sid={$v["storyid"]}\"><B>{$v["story_name"]}</B></A> by <A HREF=\"view_profile.php?aid={$v["userid"]}\">{$v["username"]}</A> &mdash; {$preview}<BR></TD></TR>";
	}
	$buffer .= "</TABLE>";
	$content["CONTENT"] = $buffer;
} else {
	$content["CONTENT"] = "No Stories at this time";
}
include("footer.php");
?>
