<?
define("LOGIN_REQUIRED",false);
include("header.php");
$template = "main.tpl";
$login_form["LOGIN_ERROR"] = "";
if ($_POST["command"] == "Login") {
	global $db;
	$auth = & new user();
	$why = "";
	$why = $auth->authenticate($_POST["Username"],$_POST["Password"]);
	if ($why != "") { // Authentication Error
		$login_form["LOGIN_ERROR"] = $why;
	} else {
		$db->load_object($auth->username_to_id($_POST["Username"]),$auth);
		$sess->register("userinfo",$auth->data);
		Header("Location: {$config["link"]}{$config["index_page"]}");
	}
}

$content["CONTENT"] = "{LOGIN_STUB}";
$tpl->set_file("LOGIN_STUB","login.tpl");
foreach($login_form as $k=>$v) {
	$tpl->set_var($k,$v);
}
$tpl->process("LOGIN_STUB","LOGIN_STUB",false,false,false,false);

include("footer.php");
?>
