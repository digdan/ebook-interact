<?
define("LOGIN_REQUIRED",false);
include("header.php");
$why = "";
//Check if they are already logged in.
if ($userinfo) { //Can't registered when already logged in :/
	Header("Location: {$config["link"]}{$config["index_page"]}");
}

//Unset all expected replacment tags
$regform["USERNAME_VALUE"] = "";
$regform["PASSWORD_VALUE"] = "";
$regform["EMAIL_VALUE"] = "";
$regform["BIO_VALUE"] = "";
$regform["AVATAR_VALUE"] = "";
$regform["DOMAIN"] = $config["domain"];

if ($_POST["command"] == "Register") {
	//Guest attemping to register.
	//Fire a new user instance.
	$registering = & new user();

	$why .= $registering->invalid_username($_POST["Username"]);
	$why .= $registering->blacklisted($_POST["Username"]);
	$why .= $registering->invalid_password($_POST["Password"]);

	//Check username availability
	if ($why != "") { //Registration errored out, retain variables
		$regform["USERNAME_VALUE"] = $_POST["Username"];
		$regform["PASSWORD_VALUE"] = $_POST["Password"];
		$regform["EMAIL_VALUE"] = $_POST["Email"];
		$regform["BIO_VALUE"] = makeFormSafe($_POST["Bio"]);
		$regform["AVATAR_VALUE"] = $_POST["Avatar"];
		$content["BODY_EXTRA"] = "onLoad=\"forceSelect('{$_POST["Avatar"]}')\"";
		if ($_POST["agree"]) {
			$regform["AGREEMENT_VALUE"] = "CHECKED";
		}
	} else { //Registration worked out, go ahead and create the user.
		$newbio = ereg_replace("'","''",$_POST["Bio"]);
		$verify = $registering->create_verify_code();
		$now = time();
		$toadd = array (
			"username"=>$_POST["Username"],
			"password"=>$_POST["Password"],
			"verify_code"=>$verify,
			"score"=>"0",
			"contributed"=>"0",
			"avatar"=>$_POST["Avatar"],
			"bio"=>$_POST["Bio"],
			"last_login"=>"0",
			"create_date"=>$now,
			"state"=>USER_STATE_UNVERIFIED,
			"email"=>$_POST["Email"]
		);

		$registering->set_value($toadd);
		if (!$db->save_object($registering)) {
			user_error("Unable to create user.");
		} else { //User Created, send  email
			$tmpid = $registering->data["id"];
			$tpl->set_file("REGISTRATION_EMAIL_HTML","registration_email_html.tpl");
			$tpl->set_file("REGISTRATION_EMAIL_TEXT","registration_email_text.tpl");
			$tpl->set_var("VERIFY",$verify);
			$tpl->set_var("ID",$tmpid);
			$tpl->set_var("DOMAIN",$config["domain"]);
			$tpl->set_var("LINK",$config["link"]);
			$tpl->set_var("APP_TITLE",$content["APP_TITLE"]);
			$tpl->set_var("USERNAME",$_POST["Username"]);
			$tpl->set_var("PASSWORD",$_POST["Password"]);
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			mail($_POST["Email"],"Verification for '{$_POST["Username"]}' at {$content["APP_TITLE"]}",$tpl->parse("REGISTRATION_EMAIL_HTML"),$headers);

			$template = "main.tpl";
			$content["CONTENT"] = "{VERIFYING}";
			$tpl->set_file("VERIFYING","verifying.tpl");
			include("footer.php");
		}
	}
}


	$template = "main.tpl";
	$content["CONTENT"] = "{REGISTER}";
	$tpl->set_file("REGISTER","register.tpl");
	$tpl->set_file("AGREEMENT","agreement.tpl");
	$tpl->set_file("HELP_REGISTER_USER","help_register_user.tpl");
	foreach($regform as $k=>$v) {
		$tpl->set_var($k,$v);
	}
	$tpl->process("HELP_REGISTER_USER","HELP_REGISTER_USER",false,false,false,false);
	$tpl->process("AGREEMENT","AGREEMENT",false,false,false,false);
	$tpl->set_var("DOMAIN",$config["domain"]);
	$tpl->set_var("WHY",$why);
	$tpl->process("REGISTER","REGISTER",false,false,false,false); //Needed to process the WHY and other inner tags

	include("footer.php");
?>
