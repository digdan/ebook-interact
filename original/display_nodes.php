<?
	include("header.php");
?>
<LINK REL=StyleSheet HREF="<?= $config["style_sheet"] ?>" TITLE="Contemporary" TYPE="text/css">
<BODY CLASS="node_window_content">
<DIV CLASS="node_window_content">

<?
	function display_node_results($results) {
		global $instance,$db;
		if (is_array($results)) {
			foreach($results as $k=>$v) {
				switch ($v["node_type"]) {
					case 1 : $typename = "Simple"; break;
				}
				$db->query("SELECT * from instances where id = {$v["tempid"]}");
				$instancename = $db->results[0]["name"];	
				$v["name"] = shorten($v["name"],40);
				$v["instance_name"] = shorten($v["instance_name"],30);
				echo "<TABLE BGCOLOR=\"{$alternative_color}\" CLASS=\"node_window_unit\">\n";
				echo "<TR><TD ALIGN=\"CENTER\"><B><A TARGET=\"_parent\" HREF=\"edit_node.php?nid={$v["id"]}\">{$v["name"]}</A></B></TD></TR>";
				echo "<TR><TD>";
				echo "Type : <I>{$typename}</I><BR>";
				echo "Instance :  <I><A TARGET=\"_parent\" HREF=\"edit_instance.php?iid={$v["tempid"]}\">{$instancename}</A></I><BR>";
				echo "</TD></TR>";
				echo "</TABLE><BR>";
			}
		} else {
			echo "<CENTER><I>None</I></CENTER><BR>";
		}
	}

	echo "<CENTER><B>Outbound Nodes</B></CENTER>";
	$db->query("SELECT nodes.*, nodes.destination_id as tempid from instances,nodes where nodes.instance_id = {$_GET["iid"]} and instances.id = nodes.instance_id");
	display_node_results($db->results);

	echo "<CENTER><B>Inbound Nodes</B></CENTER>";
	$db->query("SELECT nodes.*, nodes.instance_id as tempid from instances,nodes where nodes.destination_id = {$_GET["iid"]} and instances.id = nodes.destination_id");
	display_node_results($db->results);
	
?>
</DIV>
</BODY>
