<?
define("LOGIN_REQUIRED",false);
include("header.php");
$template = "main.tpl";
$content["CONTENT"] = "{INDEX_STUB}";

$tpl->set_file("MOTD","index_motd.tpl");
$tpl->process("MOTD","MOTD",false,false,false,false);

$index_form["NEW_STORIES"] = "";
$index_form["BEST_STORIES"] = "";
$index_form["LARGEST_STORIES"] = "";
$index_form["MOST_ACTIVE_STORIES"] = "";

if (!$userinfo) {
	$tpl->set_file("LOGIN","login.tpl");
	$tpl->set_var("LOGIN_ERROR","");
	$tpl->process("LOGIN","LOGIN",false,false,false,false);
	
} else {
	$index_form["LOGIN"] = "";
}

$tpl->set_file("INDEX_STUB","index.tpl");
foreach($index_form as $k=>$v) {
	$tpl->set_var($k,$v);
}
$tpl->process("INDEX_STUB","INDEX_STUB",false,false,false,false);
include("footer.php");
?>
