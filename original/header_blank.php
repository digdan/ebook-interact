<?
	//Include.php Main include file. Sorts out all includes const and so on

	require_once("defs.php");

	$content["LOGIN_INFO"] = "";
	$content["CONTENT"] = "";
	$content["BODY_EXTRA"] = "";
	$content["TITLE"] = "{APP_TITLE}";

	require_once(INC_DIR_PREFIX."/config.php");
	//require_once(INC_DIR_PREFIX."/errorHandler.php");
	//$error = & new ErrorHandler();
	error_reporting(E_ALL);

	require_once(INC_DIR_PREFIX."/db.php");
	$db = & new db();

	require_once(INC_DIR_PREFIX."/base.php");

	require_once(INC_DIR_PREFIX."/misc.php");

	require_once(INC_DIR_PREFIX."/users.php");

	require_once(INC_DIR_PREFIX."/danplates.php");

	require_once(INC_DIR_PREFIX."/sessions.php");

	require_once(INC_DIR_PREFIX."/stories.php");

	require_once(INC_DIR_PREFIX."/instances.php");

	require_once(INC_DIR_PREFIX."/nodes.php");

	require_once(INC_DIR_PREFIX."/photofile.php");

	require_once(INC_DIR_PREFIX."/bbcode.php");

	$sess = & new session();
	$userinfo = $sess->get("userinfo");

	if (LOGIN_REQUIRED == true) {
		//Check if they are already logged in.
		if (!$userinfo) { //Must be logged in to access this page
			Header("Location: {$config["link"]}{$config["index_page"]}");
		}
	}

	include(INC_DIR_PREFIX."/main_menu.php");

	$tpl = & new danplate();
	$tpl->set_root(TEMPLATE_DIR);
	$tpl->set_var("STYLE_SHEET",$config["style_sheet"]);
?>
