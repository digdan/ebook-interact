<?
	define("LOGIN_REQUIRED",false);
	include("header_blank.php");
	if ($_GET["sid"]) {
		//We are given a Story ID. This means we are starting a new story
		$sess->register("story_id",$_GET["sid"]);
		$sess->register("step","1");
		$story = new story($_GET["sid"]);
		$author = new user($story->data["owner_id"]);
		$nextstep = $sess->get("step")+1;
		$unique = substr($sess->id(),0,8);
		$verify = substr(md5("{$story->data["id"]}{$story->data["starting_instance_id"]}{$unique}{$nextstep}"),0,16);
		$link = "{$story->data["starting_instance_id"]}.0.{$verify}";
		
		$output .= "<TITLE>{$story->data["story_name"]}</TITLE>\n";
		$output .= "<CENTER><H1>{$story->data["story_name"]}</H1></CENTER>";
		$output .= "<CENTER><BR>By : <TABLE width=\"600\" style=\"border:1px solid black\"><TR><TD ALIGN=\"CENTER\"><IMG SRC=\"avatars/{$author->data["avatar"]}\" BORDER=\"0\"></TD></TR><TR><TD ALIGN=\"CENTER\"><A HREF=\"view_account.php?aid={$author->data["id"]}\">{$author->data["username"]}</A></CENTER>";
		$output .= "<BR><BR>";
		$converted_summary = bbcode(nl2br($story->data["story_summary"]));
		$output .= "{$converted_summary}";
		$output .= "<BR><BR>";
		$output .= "<FORM NAME=\"trans_form\" TARGET=\"_blank\" METHOD=\"POST\" ACTION=\"?\" onSubmit=\"document.location.href='{$config["link"]}';return true\">";
		$output .= "<INPUT TYPE=\"HIDDEN\" NAME=\"trans\" value=\"{$link}\">";
		$output .= "<INPUT TYPE=\"SUBMIT\" NAME=\"cmd\" value=\"Start Story\" onClick=\"this.disabled=true;\">";
		$output .= "</FORM>";

		echo $output;
	}

	if ($_POST["trans"]) {
		//Do predicted sequence checking to maintane story flow
		$story_id = $sess->get("story_id");
		$story = new story($story_id);
		
		$goodauth = true;
		
		//Verify Trans Key
		$vparts = split("\.",$_POST["trans"]);
		if (count($vparts) != 3) { //Key is malformed or non-existant.
			$goodauth = false;
		}
		
		$checkdest = $vparts[0];
		$checknode = $vparts[1];
		$checkverify = $vparts[2];

		if (($checknode == "0") and ($checkdest != $story->data["starting_instance_id"])) { //Says its first node, but its not
			$goodauth = false;
		}

		$step = $sess->get("step");
		$nextstep = $step+1;
		$prevstep = $step-1;
		$unique = substr($sess->id(),0,8);

		$prevverify = substr(md5("{$story->data["id"]}{$checkdest}{$unique}{$prevstep}"),0,16);
		$verify = substr(md5("{$story->data["id"]}{$checkdest}{$unique}{$step}"),0,16);
		$nextverify = substr(md5("{$story->data["id"]}{$checkdest}{$unique}{$nextstep}"),0,16);

		if ($verify == $checkverify) { //Resubmited -- Should be moving away from instance, but has resubmited request
			$goodauth = false;
		} else {
			if ($nextverify == $checkverify) { //Entering Instance
				$sess->register("step",$nextstep);
			} else {
				$goodauth = false;
			}
		}

		if ($goodauth == false) { //Bad bad bad.
			#$sess->finish();
			header("Location: badrequest.php");
		}


		//End Trans Verify

		$instance_id = $checkdest;
		$sess->register("instance",$instance_id);

		$instance = new instance($instance_id);
		echo "<TITLE>{$story->data["story_name"]} - {$instance->data["name"]}</TITLE>\n";
		echo "<FORM NAME=\"trans_form\" METHOD=\"POST\" ACTION=\"?\">";
		echo "<INPUT TYPE=\"HIDDEN\" NAME=\"trans\">";
		echo "</FORM>";
		echo "<TABLE ALIGN=\"CENTER\" WIDTH=\"600\"><TR><TD ALIGN=\"CENTER\"><H2>{$instance->data["name"]}</H2></TD></TR>";
		echo "<TR><TD>";
		if ($instance->data["photofile_id"]) { //Instance has an image
			echo "<DIV STYLE=\"float:left;padding:5px\"><IMG SRC=\"image_preview.php?pid={$instance->data["photofile_id"]}\"></DIV>";
		}
		echo nl2br($instance->data["description"]);
		echo "</TD></TR>";
		echo "<TR><TD><BR><BR>";

        	$db->query("SELECT nodes.*, nodes.destination_id as tempid from instances,nodes where nodes.instance_id = {$instance_id} and instances.id = nodes.instance_id");
		
		if (is_array($db->results)) {
			foreach($db->results as $k=>$v) {
				$dest = new node($v["id"]);
				$nextstep = $sess->get("step")+1;
				$unique = substr($sess->id(),0,8);
				$verify = substr(md5("{$story->data["id"]}{$dest->data["destination_id"]}{$unique}{$nextstep}"),0,16);
				$link = "{$dest->data["destination_id"]}.{$dest->data["id"]}.{$verify}";
				echo "<LI><A HREF=\"javascript:return false\" onClick=\"trans_form.trans.value='{$link}';trans_form.submit();return false;\">{$dest->data["name"]}</A></LI>";
			}
		}


		
		echo "</TD></TR>";
		echo "</TABLE>";
		
	}
?>
