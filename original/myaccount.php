<?
define("LOGIN_REQUIRED",true);
include("header.php");
$why = "";
$success = "";

//Unset all expected replacment tags
$regform["USERNAME_VALUE"] = $userinfo['username'];
$regform["PASSWORD_VALUE"] = $userinfo['password'];
$regform["EMAIL_VALUE"] = $userinfo['email'];
$regform["BIO_VALUE"] = makeFormSafe($userinfo['bio']);
$regform["AVATAR_VALUE"] = $userinfo['avatar'];
$content["BODY_EXTRA"] = "onLoad=\"forceSelect('{$userinfo['avatar']}')\"";

if ($_POST["command"] == "Save") {
	global $db;
	//Guest attemping to register.
	//Fire a new user instance.
	$updating = & new user($userinfo["id"]);

	$why .= $updating->invalid_password($_POST["Password"]);
	$why .= $updating->authenticate($userinfo["username"],$_POST["oldPassword"]);

	//Check username availability
	if ($why != "") { //Registration errored out, retain variables
		$regform["USERNAME_VALUE"] = $userinfo["username"];
		$regform["PASSWORD_VALUE"] = $_POST["Password"];
		$regform["EMAIL_VALUE"] = $userinfo["email"];
		$regform["BIO_VALUE"] = makeFormSafe($_POST["Bio"]);
		$regform["AVATAR_VALUE"] = $_POST["Avatar"];
		$content["BODY_EXTRA"] = "onLoad=\"forceSelect('{$_POST["Avatar"]}')\"";
	} else { //Registration worked out, go ahead and create the user.
		$updateinfo = array(
			"password"=>$_POST["Password"],
			"bio"=>$_POST["Bio"],
			"avatar"=>$_POST["Avatar"]
		);
		$updating->set_value($updateinfo);
		
		if (!$db->save_object($updating)) {
			$regform["USERNAME_VALUE"] = $userinfo["username"];
			$regform["PASSWORD_VALUE"] = $_POST["Password"];
			$regform["EMAIL_VALUE"] = $userinfo["email"];
			$regform["BIO_VALUE"] = makeFormSafe($_POST["Bio"]);
			$regform["AVATAR_VALUE"] = $_POST["Avatar"];
			$content["BODY_EXTRA"] = "onLoad=\"forceSelect('{$_POST["Avatar"]}')\"";
			$error = "Update failed.";
		} else {
			//Reload Sess vars
			$tmpuser = new user($userinfo["id"]);
			$sess->register('userinfo',$tmpuser->data);
			$success = "Successfully Saved Changes.";
			$regform["BIO_VALUE"] = makeFormSafe($_POST["Bio"]);
			$regform["AVATAR_VALUE"] = $_POST["Avatar"];
			$content["BODY_EXTRA"] = "onLoad=\"forceSelect('{$_POST["Avatar"]}')\"";
		}
	}
}


	$template = "main.tpl";
	$content["CONTENT"] = "{MYACCOUNT}";
	$tpl->set_file("MYACCOUNT","myaccount.tpl");
	$tpl->set_file("AGREEMENT","agreement.tpl");
	$tpl->set_file("HELP_MYACCOUNT","help_myaccount.tpl");
	foreach($regform as $k=>$v) {
		$tpl->set_var($k,$v);
	}
	$tpl->set_var("DOMAIN",$config["domain"]);
	$tpl->set_var("WHY",$why);
	$tpl->set_var("SUCCESS",$success);
	$tpl->process("HELP_MYACCOUNT","HELP_MYACCOUNT",false,false,false,false);
	$tpl->process("MYACCOUNT","MYACCOUNT",false,false,false,false); //Needed to process the WHY and other inner tags

	include("footer.php");
?>
