<?
$userinfo = $sess->get("userinfo");
if (!$userinfo) { //Guest
	$content["LOGIN_INFO"] = "Welcome <I>guest</I>. <A HREF=\"{$config["login_page"]}\">Log-in</A> / <A HREF=\"{$config["register_page"]}\">Register</A>";
} else {
	$content["LOGIN_INFO"] = "<B><I>{$userinfo["username"]}</I></B> &middot; <A HREF=\"{$config["logoff_page"]}\">Log-off</A>";
}

foreach($content as $k=>$v) {
	$tpl->set_var($k,$v);
}

$tpl->set_file("page",$template);
$tpl->process("output","page",false,false,false,false);
$html = $tpl->parse("output");
echo $html;
die();
?>
