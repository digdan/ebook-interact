<?
	define("STORY_STATE_NOSTART",0);
	define("STORY_STATE_ACTIVE",1);
	define("STORY_STATE_INACTIVE",2);
	define("STORY_STATE_DISABLED",3);

	//Stories Routines

	function genere_ddl($preselected) {
		global $db;
		if (!$db->query("SELECT id,name from genres")) {
			user_error("Unable to load genres");
		} else {
			if ($db->numrows == 0) {
				user_error("No generes");
				return "None";

			} else {
				$out = "<SELECT name=\"genre\">\n";
		
				foreach($db->results as $k=>$v) {
					if ($v) {
						$out .= "<OPTION VALUE=\"{$v["id"]}\"";
						if ($preselected == $v["id"]) {
							$out .= "SELECTED";
						}
						$out .= ">{$v["name"]}</OPTION>\n";
					}
				}
				$out .= "</SELECT>";
				return $out;
			}
		}
	}


	class story extends base_object {

		function story($id=0) {
			global $db;
			$this->db_table = "stories";
			$this->db_map = array(
				"story_name"=>DB_QUOTED,
				"owner_id"=>DB_UNQUOTED,
				"starting_instance_id"=>DB_UNQUOTED,
				"story_summary"=>DB_QUOTED,
				"Status"=>DB_UNQUOTED,
			);
			if ($id != 0) { //Load Story
				if ($db->load_object($id,$this)) {
					return true;
				} else {
					user_error("Unable to load story.");
					return false;
				}
			}
		}

		function renderInstances($elementName,$default="",$more="") {
			global $db;
			$currentInstance = new instance();
			$db->query("SELECT id from instances where story_id = {$this->data["id"]}");
			if (is_array($db->results)) {
				$output = "<SELECT name=\"{$elementName}\" {$more}>";
				foreach($db->results as $k=>$v) {
					$db->load_object($v["id"],$currentInstance);
					if ($this->data["starting_instance_id"] == $v["id"]) {
						$xtra = "* ";
					} else {
						$xtra = "";
					}
					if ($default == $v["id"]) {
						$def = "SELECTED";
					} else {
						$def = "";
					}
					$curname = shorten($xtra.$currentInstance->data["nickname"],35);
					$output .= "<OPTION value=\"{$v["id"]}\" {$def}>{$curname}</OPTION>\n";
				}
				$output .= "</SELECT>\n";
				return $output;
			} else {
				return false;
			}
		}

		function remove() {
			global $db;
			//Gather Instances first
			$db->query("SELECT * from instances where story_id = {$this->data["id"]}");
			if (is_array($db->results)) {
				foreach($db->results as $k=>$v) {
					$instance = new instance($v["id"]);
					$output .= $instance->remove();
				}
			}
			$db->query("DELETE from stories where id = {$this->data["id"]}");
			$output .= "Story '{$this->data["story_name"]}' removed.\n";
			return $output;
		}
	}
?>
