<?
	class photofile {
		var $img;
		var $img_data;	
	
		function photofile($predef = "") {
			global $db;
			$this->db_map = array(
				"filename" => QUOTABLE,
				"size" => UNQUOTABLE,
				"instance_id" => UNQUOTABLE,
				"mime" => QUOTABLE,
				"owner_id" => UNQUOTABLE,
				"create_dat" => UNQUOTABLE
			);

			if (is_numeric($predef)) {
				$db->load_object($predef,$this);
			}
			if (is_array($predef)) {
				$this->process($predef);
			}
		}
		
		function process($imgarr) { //Loads, and manipulates an image.
			global $config;

			$this->data["mime"] = $imgarr["type"];
			$this->data["size"] = $imgarr["size"];
			$this->data["filename"] = $imgarr["name"];
			$img = null; //Start from scratch
			switch ($this->data["mime"]) {
				case "image/jpg" :
					$img = imagecreatefromjpeg($imgarr["tmp_name"]);
				break;
				case "image/jpeg" :
					$img = imagecreatefromjpeg($imgarr["tmp_name"]);
				break;
				case "image/gif" :
					$img = imagecreatefrompng($imgarr["tmp_name"]);
				break;
				case "image/png" :
					$img = imagecreatefrompng($imgarr["tmp_name"]);
				break;
				default :
					$this->data["mime"] = "image/jpeg";
					$img = imagecreatefromjpeg($imgarr["tmp_name"]);
				break;
			}

			if ($img) {
				$width = imagesx($img);
				$height = imagesy($img);
				$scale = min($config["image_maxw"]/$width, $config["image_maxh"]/$height);
				if ($scale > 1) {
					$new_width = floor($scale*$width);
					$new_height = floor($scale*$height);
					$tmp_img = imagecreatetruecolor($new_width,$new_height);
					imagecopyresized($tmp_img, $img,0,0,0,0,$new_width,$new_height,$width,$height);
					imagedestroy($img);
					$img = $tmp_image;
				}
			} else {
				$img = imagecreatefromjpeg("images/missing.jpeg");
			}
			$this->img = $img;
			$this->render();
		}

		function display() {
			Header("Content-type: {$this->data["mime"]}");
			echo $this->img_data;
		}

		function render() {
			ob_start(); // start a new output buffer
			imagejpeg($this->img, NULL, 100 );
			$ImageData = ob_get_contents();
			ob_end_clean; // stop this output buffer
			$this->img_data = $ImageData;
		}

	}
?>
