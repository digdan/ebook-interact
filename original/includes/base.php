<?
	class base_object {
		var $data;
		var $db_map;
		var $db_table;
		var $hash;

		function base_object() {
			user_error("base_object constructor not overwritten.");
		}
		
		function set_id($newid) {
			$this->data["id"] = $newid;
		}
		
		function buildDataString() {
			unset($buffer);
			foreach($this->data as $k=>$v) {
				if (($k == "create_date") or ($k == "last_updated")) {
				} else {
					$buffer .= "{$k}{$v}";
				}
			}
			return $buffer;
		}

		function set_value($key,$value=0) {
			if (is_array($key)) {
				foreach($key as $k=>$v) {
					$this->set_value($k,$v);
				}
			} else {
				$this->data[$key] = $value;
			}
			if (is_array($this->data)) {
				$this->hash = sha1($this->buildDataString());
			}
		}

		function save() {
			//Container Function
		}
	}
?>
