<?
class session {
     function session ($lifetime=10800) { // default lifetime: 3 hours
	global $db,$config;
          session_start(); 
	  $ruri = ereg_replace("'","''",$_SERVER["REQUEST_URI"]);
          $id_session=$this->id();
	  if (!$db->query("DELETE from {$config["sessions_table"]} where moment < ".(mktime()-$lifetime))) {
		user_error("Unable to delete expired sessions.");
	  }
	  if (!$db->query("SELECT moment from {$config["sessions_table"]} where id_session = '$id_session' ORDER BY moment DESC")) {
		user_error("Unable to select sessions");
	  }
	  if ($db->numrows > 0) {
		if (!$db->query("UPDATE {$config["sessions_table"]} set moment = ".mktime().", action='{$ruri}' where id_session = '{$id_session}' and moment = {$db->results[0]["moment"]}")) {
			user_error("Unable to update session.");
		}
		
	  } else {
		if (!$db->query("INSERT into {$config["sessions_table"]} (id_session,moment,name,data,action) values ('{$id_session}',".mktime().",\"\",\"\",'{$ruri}')")) {
			user_error("Unable to create session");
		}
	  }
     } 

     function register($name,$data) { 
	  global $db,$config;
          $id_session=$this->id(); 
	  $db->query("SELECT id_session,name from {$config["sessions_table"]} where (id_session='{$id_session}') and (name='')");
	  if ($db->numrows == 1) {
		if (!$db->query("update {$config["sessions_table"]} set name = '{$name}', data = '".base64_encode(serialize($data))."' where id_session='{$id_session}'")) {
			user_error("Unable to register session variable");
		}
	  } else {
		if (!$db->query("SELECT name from {$config["sessions_table"]} where (id_session='{$id_session}') AND (name='$name')")) {
			user_error("Unable to check session variable registration.");
		}
		if ($db->numrows > 0) {
			if (!$db->query("update {$config["sessions_table"]} set name = '{$name}', data = '".base64_encode(serialize($data))."' where id_session='{$id_session}' and name = '{$db->results[0]["name"]}'")) {
				user_error("Unable to check existing session variable registeration.");
			}
		
		} else { 
			if (!$db->query("INSERT into {$config["sessions_table"]} (id_session,moment,name,data) values ('{$id_session}',".mktime().",'{$name}','".base64_encode(serialize($data))."')")) {
			}
		}
          }
          return $data; 
     } 

    function unregister($name) { 
	  global $db,$config;
          $id_session=$this->id();
	  if (!$db->query("SELECT id_session from {$config["sessions_table"]} where (id_session = '{$id_session}')")) {
		user_error("Unable to unregister session variable.");
	  }
	  if ($db->numrows == 1) { //Update
		if (!$db->query("UPDATE {$config["sessions_table"]} set name = \"\", data = \"\" where id_session = '{$id_session}'")) {
			user_error("Unable to clear session variable.");
		}
	  }
	  if ($db->numrows > 1) {
		if (!$db->query("DELETE from {$config["sessions_table"]} where  id_session = '{$id_session}' and name='{$name}'")) {
			user_error("Unable to delete session variable.");
		}
	  }
     }

     function get($name,$default="") { 
	  global $db,$config;
          $id_session=$this->id();     
	  if (!$db->query("SELECT data from {$config["sessions_table"]} where id_session='{$id_session}' and name='{$name}'")) {
		user_error("Unable to retrieve session variable.");
	  }
	  if ($db->numrows > 0) {
		$dataraw = base64_decode($db->results[0]["data"]);
		$data = unserialize($dataraw);
		if ($data == false) {
			user_error("Session variable appears to be corrupt.");
		}
		return $data;
	  } else {
		return false;
	  }
     } 

     function id() { 
          return(@session_id()); 
     } 
		
     function finish() { 
	  global $db,$config;
          $id_session = $this->id(); 
	  if (!$db->query("delete from {$config["sessions_table"]} where id_session='{$id_session}'")) {
		user_error("Unable to remove session.");
	  }
          @session_unset();
          @session_destroy();
     }
		
} 

?>
