<?
	class instance extends base_object {
	/*
		Instance are the "rooms", or stopping points used in Fantacosm. They are connected by nodes.
	*/
		function instance($loadid = "") {
			global $db;
			$this->db_table = "instances";
			$this->db_map = array(
				"name"=>DB_QUOTED,
				"description"=>DB_QUOTED,
				"story_id"=>DB_UNQUOTED,
				"owner_id"=>DB_UNQUOTED,
				"flags"=>DB_UNQUOTED,	
				"create_date"=>DB_UNQUOTED,
				"last_visit"=>DB_UNQUOTED,
				"last_updated"=>DB_UNQUOTED,
				"nickname"=>DB_QUOTED,
				"photofile_id"=>DB_UNQUOTED
			);
			if ($loadid != "") {
				$db->load_object($loadid,$this);
			}
		}

		function remove() {
			global $db;
			//Find any attached Nodes
			$db->query("SELECT * from nodes where instance_id = {$this->data["id"]}");
			if (is_array($db->results)) {
				foreach($db->results as $k=>$v) {
					$node = new node($v["id"]);
					$output .= $node->remove();
				}
			}
			$db->query("DELETE from instances where id = {$this->data["id"]}");
			$output .= "Instance '{$this->data["name"]}' removed.\n";
			return $output;
		}
	}
?>
