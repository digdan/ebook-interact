<?
	//Unsortable routines

function makeFormSafe($input) { //Makes RTE content compatible with Mysql and RTE
/*
	$tmp = ereg_replace("'","&rsquo;",$input);
	$tmp = ereg_replace("\n","",$tmp);
	$tmp = ereg_replace("\r","",$tmp);
	return $tmp;
*/
	return $input;
}

function shorten($intext,$allowednumber=15) {

	if (strlen($intext) > $allowednumber) {
		$output = substr($intext,0,($allowednumber-3))."...";
	} else {
		$output = $intext;
	}
	return $output;
}

function random_filename($flength=32) {
	/*
		To maximize possible filesnames a 32-character
	*/
	$avail = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	for ($i=0;$i<$flength;$i++) {
		$output .= $avail[rand(0,strlen($avail)-1)];
	}
	return "{$output}";
}

function rand_file($path,$flength=32) { //Generate random filename until one doesn't exist.
	if (substr($path,strlen($path)-1,1) != "/") {
		$path .= "/";
	}

	$try = random_filename($flength);

	while (file_exists($path.$try)) {
		$try = random_filename($flength);
	}
	return array($path,$try);
}

?>
