<?
//Simple database class
DEFINE("QUOTED",0);
DEFINE("UNQUOTED",1);
class db {
	var $dbi,$query,$q,$results,$numrows,$lid;

	function db() {
		global $config;
		$this->dbi = mysql_connect($config["db_host"],$config["db_user"],$config["db_password"]);
		if (!$this->dbi) {
			trigger_error("Unable to connect to database server.");
		}
		$dbchange = mysql_select_db($config["db_database"],$this->dbi);
		if (!$dbchange) {
			trigger_error("Unable to select database.");
	  	}
		return true;
	  }

	function query($query) {
		unset($this->results);
		$this->numrows = "0";
		$this->query = $query;
		$this->q = mysql_query($this->query,$this->dbi);
		if (!$this->q) {
			$reason = mysql_error($this->dbi);
			user_error("Bad query : {$reason}");
			user_error("Query : {$query}");
			return false;
		}
		if (!is_resource($this->q)) {
			$this->numrows = "0";
			return true;
		}
		$this->numrows = mysql_num_rows($this->q);
		if ($this->numrows == 0) {
			return true;
		} else {
			while($buffer = mysql_fetch_array($this->q)) {
				$this->results[] = $buffer;
			}
			return true;
		}
	}

	function lid() {
		if (!$this->query("SELECT LAST_INSERT_ID();")) {
			user_error("Unable to retrieve last insert id.");
		}
		return $this->results[0][0];
	}


	function save_object(&$object,$debug=false) {
	/*
		Savable Objects have their data stored in $object->data, and their data definitions in $object->db_map
	*/
		$object->save();
		if (!$object->data["id"]) {  //Is this a new object?
			$buffer = "(";
			$ins = "(";
			foreach($object->data as $k=>$v) {
				if ($object->db_map[$k]) { //If value is defined in our db_map
					if ($object->db_map[$k] == DB_QUOTED) { //If value is a string/text/quotable value
						$ins .= "{$k},";
						$v = mysql_escape_string(stripslashes($v));
						$buffer .= "'{$v}',";
					}
					if ($object->db_map[$k] == DB_UNQUOTED) { //If value is a numerical value
						$ins .= "{$k},";
						$buffer .= "{$v},";
					}
				}
			}
			/* Include hash */
			if ($object->hash) {
				// Check hash
				$this->query("SELECT * from {$object->db_table} where hash = '{$object->hash}'");
				if ($this->numrows > 0) { //Not Unique
					/* Consider this a critical failure, kick them back to the index */
					user_error("Repeat insert detected.");
					return false;
				}
				$ins .= "hash)";
				$buffer .= "'{$object->hash}')";
			} else {
				$ins = substr($ins,0,strlen($ins)-1).")";
				$buffer = substr($buffer,0,strlen($buffer)-1).")";
			}
			$query = "INSERT into {$object->db_table} {$ins} values {$buffer}";
			if ($debug) {
				user_error($query);
			}
			if (!$this->query($query)) {
				user_error("Unable to add new object to database.");
				return false;
			} else {
				$object->set_id($this->lid()); //Set the new objects identifier
				return true;
			}
		} else { //Add Update Code Here
			foreach($object->data as $k=>$v) {
				if ($object->db_map[$k]) {
					if ($object->db_map[$k] == DB_QUOTED) {
						$v = mysql_escape_string(stripslashes($v));
						$output .= "{$k} = '{$v}',";
					}
	
					if ($object->db_map[$k] == DB_UNQUOTED) {
						$output .= "{$k} = {$v},";
					}
				}
			}
			$output = substr($output,0,strlen($output)-1);
			$query = "UPDATE {$object->db_table} set {$output} where id = {$object->data["id"]}";
			if ($debug) {
				user_error($query);
			}
			if (!$this->query($query)) {
				user_error("Unable to update object");
				return false;
			} else {
				return true;
			}
		}
	}

	function load_object($id,&$object) {
		$query = "SELECT * from {$object->db_table} where id = {$id}";	
		if (!$this->query($query)) {
			user_error("Unable to load object from database");
			return false;
		} else {
			if (is_array($this->results[0])) {
				foreach ($this->results[0] as $k=>$v) {
					if (!is_numeric($k)) { //Load named, not numeric keys
						$object->set_value($k,$v);
					}
				}
			} 
			$object->set_id($id);
			return true;
		}
	}
}
?>
