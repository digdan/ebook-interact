<?
function bbcode($string){
        $string = nl2br(htmlspecialchars($string));
        $patterns = array(
               '`\[b\](.+?)\[/b\]`is',
                '`\[i\](.+?)\[/i\]`is',
                '`\[u\](.+?)\[/u\]`is',
                '`\[strike\](.+?)\[/strike\]`is',
                '`\[color=#([0-9]{6})\](.+?)\[/color\]`is',
                '`\[email\](.+?)\[/email\]`is',
                '`\[img\](.+?)\[/img\]`is',
                '`\[url=([a-z0-9]+://)([\w\-]+\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*?)?)\](.*?)\[/url\]`si',
                '`\[url\]([a-z0-9]+?://){1}([\w\-]+\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*)?)\[/url\]`si',
                '`\[url\]((www|ftp)\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*?)?)\[/url\]`si',
                '`\[flash=([0-9]+),([0-9]+)\](.+?)\[/flash\]`is',
                '`\[quote\](.+?)\[/quote\]`is',
                '`\[indent](.+?)\[/indent\]`is',
                '`\[size=([1-6]+)\](.+?)\[/size\]`is'
        );
        $replaces =  array(
                '<strong>\\1</strong>',
                '<em>\\1</em>',
                '<span style="border-bottom: 1px dotted">\\1</span>',
                '<strike>\\1</strike>',
                '<span style="color:#\1;">\2</span>',
                '<a href="mailto:\1">\1</a>',
                '<img src="\1" alt="" style="border:0px;" />',
                '<a href="\1\2">\6</a>',
                '<a href="\1\2">\1\2</a>',
                '<a href="http://\1">\1</a>',
                '<object width="\1" height="\2"><param name="movie" value="\3" /><embed src="\3" width="\1" height="\2"></embed></object>',
                '<strong>Quote:</strong><div style="margin:0px 10px;padding:5px;background-color:#F7F7F7;border:1px dotted #CCCCCC;width:80%;"><em>\1</em></div>'       ,
                '<p style="text-indent:30px">\\1</p>',
                '<h\1>\2</h\1>'
        );

        $string = preg_replace($patterns, $replaces , $string);

        return $string;
}
?>
