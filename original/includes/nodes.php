<?
	class node extends base_object {
	/*
		Instance are the "rooms", or stopping points used in Fantacosm. They are connected by nodes.
	*/
		function node($id = 0) {
			global $db;
			$this->db_table = "nodes";
			$this->db_map = array(
				"name"=>DB_QUOTED,
				"node_type"=>DB_UNQUOTED,
				"instance_id"=>DB_UNQUOTED,
				"destination_id"=>DB_QUOTED,
				"owner_id"=>DB_UNQUOTED,
				"create_date"=>DB_UNQUOTED,
				"last_updated"=>DB_UNQUOTED,
			);
			if ($id != 0) {
				$db->load_object($id,$this);
			}
		}


		function remove() {
			global $db;
			$db->query("DELETE from {$this->db_table} where id = {$this->data["id"]}");
			$output .= "Node '{$this->data["name"]}' removed.\n";
			return $output;
		}
	}
?>
