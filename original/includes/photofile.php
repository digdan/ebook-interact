<?
	class photofile extends base_object {
		var $thumb;
		var $processed;
	
		function photofile($predef = "") {
			global $db;
			$this->db_table = "photofiles";
			$this->db_map = array(
				"filename" => DB_QUOTED,
				"size" => DB_UNQUOTED,
				"mime" => DB_QUOTED,
				"owner_id" => DB_UNQUOTED,
				"create_date" => DB_UNQUOTED,
				"uname" => DB_QUOTED
			);

			if ($predef == "") {
				return;
			}			

			if (is_numeric($predef)) {
				$db->load_object($predef,$this);
			}
			if (is_array($predef)) {
				$this->process($predef);
			}
		}

		function loadByUname($uname) {
			global $db;
			if (!$uname) {
				return false;
			}
			$db->query("SELECT id from {$this->db_table} where uname = '{$uname}'");
			if (!$db->results[0]["id"]) {
				return false;
			}
			$temp_id = $db->results[0]["id"];
			$db->load_object($temp_id,$this);
			return true;
		}

		
		function process($imgarr) { //Loads, and manipulates an image.
			global $config;

			$this->set_value("mime",$imgarr["type"]);
			$this->set_value("size",$imgarr["size"]);
			$this->set_value("filename",$imgarr["name"]);
			//ToDo- Thumb and Watermark this biatch
			$cmdstr = "/www/bin/thumb.php {$imgarr["tmp_name"]} {$config["watermark"]}";
			$info = exec($cmdstr);
		
			$iparts = split("\|",trim($info));
			$this->thumb = $iparts[0];
			$this->processed = $iparts[1];	
			
			$target = rand_file($config["user_images"]);
			$this->set_value("uname",$target[1]);
			
			copy($this->thumb,$target[0].$target[1].".small");
			copy($this->processed,$target[0].$target[1]);
		}
	
		function save_image() {
			global $config;
			exec("mv {$this->thumb} {$config["user_images"]}{$this->data["id"]}.small.jpeg");
			exec("mv {$this->processed} {$config["user_images"]}{$this->data["id"]}.jpeg");
		}

		function display($small=0) {
			global $config;
			if ($small == 0) {
				 $fpath = $config["user_images"].$this->data["uname"];
			} else {
				$fpath = $config["user_images"].$this->data["uname"].".small";
			}

			$filesize = filesize($fpath);
			$fp = fopen($fpath,'rb');
			header("Content-Type: {$this->data["mime"]}");
			header("Content-Length: " . filesize($fpath));
			fpassthru($fp);
			fclose($fp);
		}

		function remove() { //Delete from database and file system
			global $db,$config;
			if (file_exists("{$config["user_images"]}{$this->data["uname"]}")) {
				unlink("{$config["user_images"]}{$this->data["uname"]}");
			}
			if (file_exists("{$config["user_images"]}{$this->data["uname"]}.small")) {
				unlink("{$config["user_images"]}{$this->data["uname"]}.small");
			}
			$db->query("DELETE from {$this->db_table} where id = {$this->data["id"]}");
		}
	}
?>
