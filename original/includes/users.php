<?
class user extends base_object {

	function user($user_id=false) {
		global $db;
		$this->db_table = "users";
		$this->db_map = array(
			"username"=>DB_QUOTED,
			"password"=>DB_QUOTED,
			"verify_code"=>DB_QUOTED,
			"score"=>DB_UNQUOTED,
			"contributed"=>DB_UNQUOTED,
			"avatar"=>DB_QUOTED,
			"bio"=>DB_QUOTED,
			"last_login"=>DB_UNQUOTED,
			"state"=>DB_UNQUOTED,
			"email"=>DB_QUOTED,
			"create_date"=>DB_UNQUOTED
		);

		if ($user_id) { //They need to load a user.
			return $db->load_object($user_id,$this);
		} else {
			return true;
		}
	}

	function username_to_id($username) {
		global $db;
		if (!$db->query("SELECT id from users where username = '{$username}'")) {
			user_error("Unable to complete name to id lookup.");
		} else {
			if ($db->numrows == 0) {
				return false;
			} else {
				return $db->results[0][0];
			}
		}
	}

	function blacklisted($attempted) {
		global $config;
		$blacklist = file($config["blacklist"]);
		$tocomp = strtolower($attempted);
		foreach($blacklist as $k=>$v) {
			$comp = trim(strtolower($v));
			if ($comp == $tocomp) {
				return "Sorry, that username nas been blacklisted.";
			}
		}
	}

	function invalid_username($attempted) {
		global $db;
		$db->query("SELECT * from users where username = '{$attempted}'");
		if ($db->numrows > 0) {
			return "Username already in use.<BR>";
		}
		if (strlen($attempted) < 4) {
			return "Username too short.<BR>";
		}
		if (strlen($attempted) > 10) {
			return "Username too long.<BR>";
		}
		if (!eregi("^[a-z0-9_-]{4,10}$",$attempted)) {
			return "Invalid characters in username.<BR>";
		}
	}

	function invalid_password($attempted) {
		if (strlen($attempted) < 6) {
			return "Password is too short.<BR>";
		}
		if (strlen($attempted) > 16) {
			return "Password is too long.<BR>";
		}
		if (!eregi("^[a-z0-9_-]{4,18}$",$attempted)) {
			return "Invalid characters in password.<BR>";
		}
	}

	function create_verify_code() {
		$randstr='';
		$length = 16;
		srand((double)microtime()*1000000);
		$chars = array ( 'a','b','c','d','e','f','0','1','2','3','4','5','6','7','8','9');
		for ($rand = 0; $rand <= $length; $rand++) {
			$random = rand(0, count($chars) -1);
			$randstr .= $chars[$random];
		}
		return $randstr;
	}

	function verify($id,$code) {
		global $db;
		if (!$db->query("SELECT verify_code,state from users where id ={$id}")) {
			return "Unable to verify account.";
		}
		if ($db->results[0]["state"] != USER_STATE_UNVERIFIED) {
			return "Account already verified, or can not be verified.";
		}
		if ($db->results[0]["verify_code"] != $code) {
			return "Invalid account verification.";
		}

		if (!$db->query("UPDATE users set state = ".USER_STATE_ACTIVE." where id = {$id}")) {
			return "Unable to activate account.";
		}
	}

	function authenticate($username,$password) {
		global $db;
		if (!$db->query("SELECT id,state,password from users where username = '{$username}'")) {
			return "Unable to authenticate at this time";
		}
		if ($db->numrows == 0) {
			return "Invalid username/password combination.";
		}
		if ($db->results[0]["state"] != USER_STATE_ACTIVE) {
			return "Account is unactivated, disabled, or banned.";
		}
		if ($db->results[0]["password"] != $password) {
			return "Invalid username/password combination.";
		}
	}
}
?>
