<?
	/*
		Bit Vector Defs
	*/

	define("USER_STATE_UNVERIFIED",0); //Before verified
	define("USER_STATE_ACTIVE",1); //Only state that can login
	define("USER_STATE_BANNED",2);

	define("INC_DIR_PREFIX","includes"); //Prefix to includes
	define("TEMPLATE_DIR","templates/");

	define("DB_QUOTED",1);
	define("DB_UNQUOTED",2);
?>
