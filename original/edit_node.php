<?
define("LOGIN_REQUIRED",true);
include("header.php");

$edit["error"] = "";
if ($_GET["iid"]) {
	// Add ownership checking here
	$sess->register("instance_id",$_GET["iid"]);
}

$edit["NODE_NAME"] = "";
$edit["NODE_TYPE"] = "";
$edit["NODE_DATA"] = "";
if ($_POST["NODE_ID"]) {
	$nodeid = $_POST["NODE_ID"];
} else {
	if ($_GET["nid"]) {
		$nodeid = $_GET["nid"];
	}
	$edit["NODE_ID"] = "";
}

/* Now Load the Story */
$storyid = $sess->get("story_id");
$story = new story($storyid);

/* now Load the FROM instance */
$instance_id = $sess->get("instance_id");
$instance = new instance($instance_id);

if ($_POST["command"] == "Save Node") {
		global $db;
		$newname = ereg_replace("'","''",$_POST["name"]);
		$ownerid = $userinfo["id"];
		if (!$ownerid) {
			user_error("Unable to allocate owner.");
		}

		//Where we configure the data for the node.
		//If Instance_Type =
		switch($_POST["Node_Type"]) {
			case 1 :
				$renderedData = $_POST["Destination_Instance"];
			break;
		}

		$now = time();
		if ($_POST["NODE_ID"]) {
			$values = array (
				"name"=>$newname,
				"node_type"=>$_POST["Node_Type"],
				"instance_id"=>$instance_id,
				"owner_id"=>$ownerid,
				"destination_id"=>$renderedData,
				"last_updated"=>$now,
				"id"=>$_POST["NODE_ID"]

			);
		} else {
			$values = array (
				"name"=>$newname,
				"node_type"=>$_POST["Node_Type"],
				"instance_id"=>$instance_id,
				"owner_id"=>$ownerid,
				"destination_id"=>$renderedData,
				"last_updated"=>$now,
				"create_date"=>$now,
			);
		}
		$node = new node();
		$node->set_value($values);

                if (!$db->save_object($node)) {
						echo mysql_error();
                        $edit["error"] = "Unable to save Node.";
                        $edit["NODE_NAME"] = $_POST["name"];
                        $edit["DESTINATION_INSTANCE"] = $_POST["Destination_Instance"];
                } else { //Successful Save
			if ($node->data["destination_id"] == "0") {
				Header("Location: edit_instance.php?sid={$storyid}&nid={$node->data["id"]}");
			} else {
				Header("Location: edit_instance.php?sid={$storyid}&iid={$node->data["instance_id"]}");
			}

                }

		if (!$_POST["NODE_ID"]) {
			$edit["NODE_ID"] = $instance->data["id"];
		}
		$edit["NODE_NAME"] = $node->data["name"];
		$edit["DESTINATION_INSTANCE"] = $node->data["destination_id"];
		$edit["NODE_TYPE"] = $node->data["node_type"];
		$edit["NODE_ID"] = $node->data["id"];
}

if ($nodeid) {
	$node = new node();
	$db->load_object($nodeid,$node);
	$edit["NODE_NAME"] = $node->data["name"];
	$edit["DESTINATION_INSTANCE"] = $node->data["destination_id"];
	$edit["NODE_TYPE"] = $node->data["node_type"];
	$edit["NODE_ID"] = $node->data["id"];
}


$other_instances = $story->renderInstances("instance_id",$node->data["destination_id"]);
$edit["OTHER_INSTANCES"] = $other_instances;

$storyinfo = "<TABLE class=\"editor_storymenu\"><TR><TD ALIGN=\"left\"><B>Story :</B> {$story->data["story_name"]} <SMALL>[<A HREF=\"edit_story.php?sid={$story->data["id"]}\">edit</A>]</SMALL></TD>";

$instance_list = $story->renderInstances("instance_id",$instance->data["id"],"onChange=\"document.location.href='edit_instance.php?iid='+this.value;\"");

$storyinfo .= "<TD ALIGN=\"right\">Current Instance : {$instance_list}</TR></TABLE>";
$tpl->set_var("STORY_INFO",$storyinfo);


$template = "main.tpl";
$content["CONTENT"] = "{EDIT_STUB}";

$tpl->set_file("EDIT_STUB","edit_node.tpl");
foreach($edit as $k=>$v) {
	$tpl->set_var($k,$v);
}
$tpl->set_file("HELP_EDIT_NODE","help_edit_node.tpl");
$tpl->process("HELP_EDIT_NODE","HELP_EDIT_NODE",false,false,false,false);
$tpl->process("EDIT_STUB","EDIT_STUB",false,false,false,false);
include("footer.php");
?>
