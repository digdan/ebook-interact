<?
include("header.php");
$template = "main.tpl";
$test = $sess->get('userinfo');
if ($test) {
	$sess->finish();
	$content["CONTENT"] = "You have been logged off.";
	$content["MEMBER_MENU"] = "";
} else {
	$content["CONTENT"] = "You must log-in first.";
}
include("footer.php");
?>
