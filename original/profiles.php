<?
include("header.php");
$template = "main.tpl";
	$profiled = &new user();
	if (!$profiled->loaduser($_GET["uid"])) {
		user_error("Unable to load user.");
	}
$content["CONTENT"] = "{PROFILES_STUB}";
$tpl->set_file("PROFILES_STUB","profiles.tpl");

$tpl->set_var("AVATAR","avatars/{$profiled->data["avatar"]}");
$tpl->set_var("USERNAME",$profiled->data["username"]);
$tpl->set_var("BIO",$profiled->data["bio"]);

$stats = "<B><I>Score : </I></B>".$profiled->data["score"]."<BR>";
$stats .= "<B><I>Stories : </I></B>".$profiled->data["stories"]."<BR>";
$stats .= "<B><I>Pages : </I></B>".$profiled->data["pages"]."<BR>";
$stats .= "<BR><BR>";
$stats .= "<B><I>Last Login : </I></B>".date("h:i:sA d/m/Y",$profiled->data["last_login"])."<BR>";

$tpl->set_var("STATS",$stats);

$tpl->process("PROFILES_STUB","PROFILES_STUB",false,false,false,false);
include("footer.php");
?>
