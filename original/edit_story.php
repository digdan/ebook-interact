<?
define("LOGIN_REQUIRED",true);
include("header.php");

$edit["error"] = "";
$edit["STORY_NAME"] = "";
$edit["SUMMARY_VALUE"] = "";
$edit["STORY_ID"] = "";
$edit["INSTANCES"] = "No Instances Available.";
$edit["STORY_CONTROL"] = "";

if ($_GET["sid"]) {
	global $db;
	$story = new story($_GET["sid"]);
	$edit["STORY_NAME"] = $story->data["story_name"];
	$edit["SUMMARY_VALUE"] = makeFormSafe($story->data["story_summary"]);
	$edit["STORY_ID"] = $story->data["id"];
	$sess->register("story_id",$_GET["sid"]);
	$instanceBuffer = $story->renderInstances("instance_id",$story->data["starting_instance_id"]);
	if (!$instanceBuffer) {
		$edit["INSTANCES"] = "<CENTER><A HREF=\"edit_instance.php?sid={$story->data["id"]}\">Create Starting Instance</A></CENTER>";
	} else {
		$edit["INSTANCES"] = $instanceBuffer;
		$edit["INSTANCES"] .= "<BR><CENTER><Button onClick=\"document.location.href='edit_instance.php?iid='+storyform.instance_id.value;return false;\">Edit Instance</BUTTON></CENTER>";
	}
	$edit["STORY_CONTROL"] .= "<BR><BR><CENTER><Button onClick=\"if (confirm('Are you sure you wish to delete this story, and all its instances and nodes?')) { document.location.href='remove_story.php?sid={$_GET["sid"]}';return false;} else { return false;}\">DELETE STORY</BUTTON>";
	switch ($story->data["Status"]) {
		case 0 :
			$STATUS_DEVELOPMENT = "SELECTED";
		break;
		case 1 :
			$STATUS_ACTIVE = "SELECTED";
		break;
		case 2 :
			$STATUS_CLOSED = "SELECTED";
		break;
	}

	$edit["STORY_CONTROL"] .= "<BR><BR><CENTER>Story Status :<BR><SELECT name=\"Status\"><Option value=\"0\" {$STATUS_DEVELOPMENT}>In Development</OPTION><OPTION value=\"1\" {$STATUS_ACTIVE}>Active</OPTION><OPTION value=\"2\" {$STATUS_CLOSED}>Closed</OPTION></SELECT>";
}

if ($_POST["command"] == "Save Story") {
		global $db;
		$newname = ereg_replace("'","''",$_POST["name"]);
		$ownerid = $userinfo["id"];
		if (!$ownerid) {
			user_error("Unable to allocate story owner.");
		}
		$Status = $_REQUEST["Status"];
		if (!$Status) {
			$Status = "0";
		}
		$values = array (
			"story_name"=>"{$newname}",
			"owner_id"=>$ownerid,
			"starting_instance_id"=>"0",
			"story_summary"=>$_POST["Summary"],
			"Status"=>$Status
		);

		if ($_POST["STORY_ID"]) {
			$values["id"] = $_POST["STORY_ID"];
		}

		$newstory = new story();
		$newstory->set_value($values);

		if (!$db->save_object($newstory)) {
			$edit["error"] = "Unable to save story.";
			$edit["STORY_NAME"] = $_POST["name"];
			$preSummary = trim($_POST["Summary"]);
			$Status = $_POST["Status"];
			if (!$preSummary) {
				$edit["SUMMARY_VALUE"] = "";
			} else {
				$edit["SUMMARY_VALUE"] = $preSummary;
			}
		} else {
			//Relocate them to the page editor
			$sess->register("story_id",$newstory->data["id"]);
			Header("Location: {$config["instance_editor"]}?sid={$newstory->data["id"]}");
			die();
		}
}



$template = "main.tpl";
$content["CONTENT"] = "{EDIT_STUB}";

$tpl->set_file("EDIT_STUB","edit_story.tpl");
foreach($edit as $k=>$v) {
	$tpl->set_var($k,$v);
}
$tpl->set_file("HELP_EDIT_STORY","help_edit_story.tpl");
$tpl->process("HELP_EDIT_STORY","HELP_EDIT_STORY",false,false,false,false);
$tpl->process("EDIT_STUB","EDIT_STUB",false,false,false,false);
include("footer.php");
?>
