-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ebookinteract
-- ------------------------------------------------------
-- Server version	5.1.63-0ubuntu0.10.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `instances`
--

DROP TABLE IF EXISTS `instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `story_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `flags` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `last_visit` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `photofile_id` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instances`
--

LOCK TABLES `instances` WRITE;
/*!40000 ALTER TABLE `instances` DISABLE KEYS */;
INSERT INTO `instances` VALUES (1,'Watching the Night Fall','Perched apon volkswagon beatles sized slab of granite, knees bent with butt in heals the sun set begins to play with red hues across the pastel skies. With a firm nod in affirmation to his thoughts the young tribal spiritualist stands and spreads out his arms releasing the shrieking caw of a predatory bird. \r\n\r\nThe weather has grown warmer and this means that the nearby cattle will begin to migrate into lower land for low lying foliage. The ebb and flow of nature has engrained itself into the behavior of these people.\r\n\r\nAs the young spiritualist walks back to the tribe he his mind fills with fear. The low laying lands the cattle are most likely to feed are near a new town. The noise, smells, and general chaos emitting from the near city does not set well with the tribe. The elders know. They know.\r\n\r\nThat night the eldest in the tribe chose 4 of the most athletic boys to venture to the low laying grassland near the town.\r\n\r\nJeq the larger and oldest of the boys was first to be picked. He wore his age like a badge, and always assumed the leader role.\r\n\r\nHijuki was chosen second. He was very athletic and quick, but also jumpy and scrawny.\r\n\r\nWahoko was reluctantly chosen third. Wahoka was a defiant, sexist, and constantly challenging and generally rude. \r\n\r\nLoyup was hastily chosen. Not a good choice as seen by others. Loyup was not much for games, fighting, or competing. He was more like a runt who played with rocks most the day while talking to himself.\r\n\r\nThe morning shadows begin to cast giving all raised rocks deep shadows. The four boys prepare for a journey and begin down the road they would race down as children.\r\n\r\n',1,6,0,1341469548,0,1341470417,'Watching the Night Fall',0,'5823976af2f4a5cc6e8fb9eaf3454ba6'),(2,'Down the familiar road','The four boys continue to stroll casually down the road for a few hour journey to a low lying valley.',1,6,0,1341470613,0,0,'down the familiar road',0,'8d027315d5d1ed50da75f1b3266592c7'),(3,'Down the road 2','',1,6,0,1341470677,0,0,'down the road 2',0,'2717b2bc10de6e5a3e38407c0b418c93');
/*!40000 ALTER TABLE `instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodes`
--

DROP TABLE IF EXISTS `nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `node_type` int(11) NOT NULL,
  `instance_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodes`
--

LOCK TABLES `nodes` WRITE;
/*!40000 ALTER TABLE `nodes` DISABLE KEYS */;
INSERT INTO `nodes` VALUES (1,'Continue story as Jeq the strong',1,1,0,6,1341470553,1341470630,'9c52f424fdd4552a6d11636024e06f43'),(2,'Continue story as Hijuki the quick',1,1,3,6,1341470664,1341470664,'73abd60591bb355357ce38d763d153ca');
/*!40000 ALTER TABLE `nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(32) NOT NULL,
  `moment` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `data` mediumtext NOT NULL,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (6,'vr8raivekf919ba9q4kf1k1t26',1341486928,'','','/');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stories`
--

DROP TABLE IF EXISTS `stories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_name` varchar(64) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `starting_instance_id` int(11) NOT NULL,
  `story_summary` mediumtext NOT NULL,
  `Status` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stories`
--

LOCK TABLES `stories` WRITE;
/*!40000 ALTER TABLE `stories` DISABLE KEYS */;
INSERT INTO `stories` VALUES (1,'The wrongest turn',6,1,'A story about bad judgement',0,'792d04c1500c0c2a2ed476137cc6423a');
/*!40000 ALTER TABLE `stories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `verify_code` varchar(32) NOT NULL,
  `score` int(11) NOT NULL,
  `contributed` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `bio` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `email` varchar(64) NOT NULL,
  `hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'digdan','rotation','5f19cfaef029f31d0',0,0,'perejil.jpg',0,0,1341468067,1,'dan@danmorgan.net','51ee2d6836c20dc6abfac978b84d117a');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-07-06 12:01:20
