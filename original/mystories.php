<?
define("LOGIN_REQUIRED",true);
include("header.php");
$template = "main.tpl";

if (!$db->query("SELECT * from stories where owner_id = {$userinfo["id"]}")) {
	user_error("Unable to locate stories.");
}
if ($db->numrows > 0) {
	$out = "";
	$count = "0";
	foreach($db->results as $k=>$v) {
		$count++;
		if (!$v["story_name"]) {
			$v["story_name"] = "<I>No Name</I>";
		}
		$preview = shorten($v["story_summary"],300);
		$out .= "<TABLE WIDTH=\"700\">";
		$out .= "<TR><TD>{$count}.<A HREF=\"edit_story.php?sid={$v["id"]}\">{$v["story_name"]}</A> - {$preview}<BR><BR></TD></TR>";
		$out .= "</TABLE>";
	}
	$content["CONTENT"] = $out;
} else {
	$content["CONTENT"] = "You have no stories.";
}

include("footer.php");
?>
