#!/usr/bin/php -q
<?php
	if ($argv[1] == 'clean') {
		system("rm mimetype");
		system("rm work.epub");
		system("rm -rf OPS");
		system("rm -rf META-INF");
		die("Cleaned.\n");
	}
	if (!is_numeric($book_id=$argv[1])) die("First parameter must be numeric Book ID.\n");
        define('APP_ROOT',dirname(__DIR__)."/dev");
        define('DEVEL',TRUE);

        //Define library path
        set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__)."/dev" . DIRECTORY_SEPARATOR . "libs" );

        //Initiate Autoloader
        include("../dev/autoload.php");

        //Initiate Instances through Singleton Factories
        $config = Config::instance('default','../dev/config.php');
        Scope::setBase($config["paths"]["templates"]);
        $scope = Scope::instance('default');
        DB::instance('main', $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );
        $router = Router::instance('html',include '../dev/routes.php');
        $cache = Cache::instance('default',$config["cache"]["host"],$config["cache"]["port"]);
        $fkeeper = FKeeper::instance('default',$config["books_directory"]); //File Keeper


	include_once("../dev/model/books.php");
	include_once("../dev/model/ebooks.php");
	include_once("../dev/libs/FKeeper.php");

	$book = new model_books($book_id);
	$ebook = model_ebooks::loadLatest($book_id);
	$file = FKeeper::instance()->hashPath($ebook->hash);

	system("cp {$file} ./work.epub");
	system("unzip work.epub");

	
?>
